﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

// параметры оружия для отображения
[System.Serializable]
public struct WeaponShowParam {

    public Sprite sprite; // спрайт для отображения
    public string name; // название
    public int damage; // повреждение
    public int accuracy; // точность
    public int firerate; // скорострельность
    public int price; // цена
}

// магазин оружия
public class WeaponShop : MonoBehaviour, SwipeDelegate {

    // тип кнопки OK
    private enum BUTTON_OK { STANDARD = 0, DISABLED = 1, BUY = 2, BUY_DISABLED = 3 }

    // публичные переменные
    public SwipeDetector swipeDetector; // определить свайпов
    public Image okButtonImage; // изображение кнопки "OK"
    public Button okButtonButton; // кнопка кнопки "ОК"
    public Button nextButtonButton; // изображение кнопки "next"
    public Button prevButtonButton; // изображение кнопки "prev"
    public WeaponShowParam[] weaponShowParams; // параметры оружия для презентации
    public Sprite[] okButtonSprites; // кнопка "OK" стандартная(0), нажатая(1), купить(2)
    public GameObject weaponNext; // следующее оружие
    public GameObject weaponPrev; // предыдущее оружие
    public Text weaponName; // текст названия
    public Image[] damageDots; // точки-параметры повреждения
    public Image[] accuracyDots; // точки-параметры точности
    public Image[] firerateDots; // точки-параметры скорострельности
    public Sprite[] onoffDots; // исходные спрайты точек параметров (0-ON, 1-OFF)
    public Text totalMoney; // текст общего количества денег
    public Text weaponPrice; // цена активного оружия
    public GameObject priceObject; // ценник

    // личные переменные
    private int weaponNumber; // текущий отображаемый выбранный номер оружия
    private static int chosenNumber = 0; // выбранный номер оружия
    private Animation imageNextAnimation; // анимация оружия
    private Image weaponNextImage; // изображение оружия
    private Animation imagePrevAnimation; // анимация оружия
    private Image weaponPrevImage; // изображение оружия

    // пробуждение
    private void Awake() {

        // собрать параметры
        swipeDetector.del = this;
        imageNextAnimation = weaponNext.GetComponent<Animation>();
        weaponNextImage = weaponNext.GetComponent<Image>();
        imagePrevAnimation = weaponPrev.GetComponent<Animation>();
        weaponPrevImage = weaponPrev.GetComponent<Image>();

        // выбрать активный номер
        weaponNumber = chosenNumber;
        UpdateObjects(false);
    }

    // включение панели
    private void OnEnable() {

        // разное
        totalMoney.text = GlobalScoreCounter.GetMoney().ToString();
    }

    // кнопка "следующее оружие"
    public void OnNextWeaponClick() {

        // анимация ещё не закончилась
        if (imageNextAnimation.isPlaying || imagePrevAnimation.isPlaying) {
            return;
        }

        // изображения
        weaponNumber++;
        UpdateObjects(false);
        prevButtonButton.interactable = true;

        // анимация и звуки
        imageNextAnimation.Play("WeaponNextForward");
        imagePrevAnimation.Play("WeaponPrevForward");
        SoundsScript.PlaySound(SoundsScript.PAGETURN);
    }

    // кнопка "предыдущее оружие"
    public void OnPrevWeaponClick() {

        // анимация ещё не закончилась
        if (imageNextAnimation.isPlaying || imagePrevAnimation.isPlaying) {
            return;
        }

        // изображения
        weaponNumber--;
        UpdateObjects(true);
        nextButtonButton.interactable = true;

        // анимация и звуки
        imageNextAnimation.Play("WeaponNextBack");
        imagePrevAnimation.Play("WeaponPrevBack");
        SoundsScript.PlaySound(SoundsScript.PAGETURN);
    }

    // обновить объекты на экране
    private void UpdateObjects(bool back) {

        // точки
        for (int i = 0; i < weaponShowParams.Length; i++) {

            damageDots[i].sprite = onoffDots[weaponShowParams[weaponNumber].damage > i ? 0 : 1];
            accuracyDots[i].sprite = onoffDots[weaponShowParams[weaponNumber].accuracy > i ? 0 : 1];
            firerateDots[i].sprite = onoffDots[weaponShowParams[weaponNumber].firerate > i ? 0 : 1];
        }

        // разное
        weaponPrice.text = weaponShowParams[weaponNumber].price.ToString();
        weaponName.text = Localizator.GetValueForKey(weaponShowParams[weaponNumber].name);
        weaponNextImage.sprite = weaponShowParams[weaponNumber + (back ? 1 : 0)].sprite;
        weaponNextImage.SetNativeSize();
        weaponPrevImage.sprite = weaponShowParams[Mathf.Max(0, weaponNumber - (back ? 0 : 1))].sprite;
        weaponPrevImage.SetNativeSize();
        if (weaponNumber >= weaponShowParams.Length - 1) {         

            // скрыть кнопку "next"
            nextButtonButton.interactable = false;
        }
        if (weaponNumber <= 0) prevButtonButton.interactable = false;

        // внешний вид кнопки OK
        bool buttonEnabled = false;
        if (SharedPrefs.weaponSold[weaponNumber]) {

            // оружие продано
            priceObject.SetActive(false);
            if (weaponNumber == chosenNumber) okButtonImage.sprite = okButtonSprites[(int)BUTTON_OK.DISABLED];
            else {
                buttonEnabled = true;
                okButtonImage.sprite = okButtonSprites[(int)BUTTON_OK.STANDARD];
            }            
        }
        else {

            // оружие не продано
            priceObject.SetActive(true);
            if (weaponShowParams[weaponNumber].price <= GlobalScoreCounter.GetMoney()) {
                buttonEnabled = true;
                okButtonImage.sprite = okButtonSprites[(int)BUTTON_OK.BUY];
            }
            else okButtonImage.sprite = okButtonSprites[(int)BUTTON_OK.BUY_DISABLED];
        }

        // доступность кнопки
        okButtonButton.enabled = buttonEnabled;
    }

    // кнопка "OK"
    public void OnChooseWeaponClick() {

        // проданность оружия
        bool purchased = false;
        if (!SharedPrefs.weaponSold[weaponNumber]) {

            if (GlobalScoreCounter.GetMoney() >= weaponShowParams[weaponNumber].price) {

                // оружие продано
                priceObject.SetActive(false);
                SharedPrefs.WeaponSoldAndSave(weaponNumber);
                int remains = GlobalScoreCounter.SubstractMoneyAndSave(weaponShowParams[weaponNumber].price);
                totalMoney.text = remains.ToString();
                purchased = true;
            }
            else {

                // денег на оружие нет
                return;
            }
        }

        // сменить оружие
        SoundsScript.PlaySound(purchased ? SoundsScript.PURCHASE : SoundsScript.EQUIP);
        chosenNumber = weaponNumber;
        okButtonImage.sprite = okButtonSprites[(int)BUTTON_OK.DISABLED];
        okButtonButton.enabled = false;
    }

    // извлечь номер оружия
    public int GetChosenWeaponNumber() {

        return chosenNumber;
    }

    // свайпы
    void SwipeDelegate.Swipe(HorDirection direction) {

        if (direction == HorDirection.LEFT) {
            if (nextButtonButton.interactable) OnNextWeaponClick();
        }
        else {
            if (prevButtonButton.interactable) OnPrevWeaponClick();
        }
    }
}
