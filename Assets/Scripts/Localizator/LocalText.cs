﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

// пара объект-ключ
[System.Serializable]
public struct ObjectKey {
    public Text text;
    public string key;
}

// пара изображение спрайты
[System.Serializable]
public struct ImageSprite {
    public Image image;
    public Sprite rusSprite;
    public Sprite engSprite;
}

// преобразователь текста в локализаторе
public class LocalText : MonoBehaviour {

    // публичные переменные
    public ObjectKey[] objectKey;
    public ImageSprite[] imageSprite;

    // пробуждение
    private void Awake() {

        // текстовка
        foreach (ObjectKey ok in objectKey) {
            ok.text.text = Localizator.GetValueForKey(ok.key);
        }

        // изображения
        foreach (ImageSprite ims in imageSprite) {
            ims.image.sprite = Localizator.isEnglish() ? ims.engSprite : ims.rusSprite;
            ims.image.SetNativeSize();
        }
    }
}
