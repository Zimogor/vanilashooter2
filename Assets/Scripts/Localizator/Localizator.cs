﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// локализатор
public class Localizator : MonoBehaviour {

    // структура хранения языков
    private static bool english = true;
    private static List<LanguageData> languageData;
    private struct LanguageData {
        public static void Add(string key, string rus, string eng) {
            LanguageData newEntry = new LanguageData();
            newEntry.key = key;
            newEntry.rus = rus;
            newEntry.eng = eng;
            languageData.Add(newEntry);
        }
        public string key {get; private set; }
        public string value { get { return english ? eng : rus; } }
        private string rus;
        private string eng;
    }

    // личные переменные
    private static Dictionary<string, string> dictionary; // словарь

    // статический конструктор
    static Localizator() {

        // инициализация
        switch (Application.systemLanguage) {
            case SystemLanguage.Russian:
                english = false;
                break;
            default:
                english = true;
                break;
        }

        // todo
        english = true;
        
        languageData = new List<LanguageData>();
        dictionary = new Dictionary<string, string>();

        // заполнение значениями
        LanguageData.Add("CREDITS",
            "ХУДОЖНИК - ДМИТРИЙ КЛЮКИН\nПРОГРАММИСТ - СЕРГЕЙ ЗАХАРОВ\n\nОСОБАЯ БЛАГОДАРНОСТЬ\nАРТЁМУ АКМУЛИНУ\nЗА ПОТРЯСНЫЙ САУНД-ТРЕК",
            "ARTIST - DMITRY KLYUKIN\nPROGRAMMER - SERGEY ZAKHAROV\n\nSPECIAL THANKS TO ARTEM AKMULIN\nFOR AWESOME SOUNDTRACK");
        LanguageData.Add("KILL_LEVEL", "УБЕЙ ВСЕХ", "KILL THEM ALL");
        LanguageData.Add("SURVIVE_LEVEL", "ВЫЖИВАНИЕ", "SURVIVAL");
        LanguageData.Add("TIME_LEVEL", "ПРЕВОЗМОГАНИЕ", "OVERCOMING");
        LanguageData.Add("RECORDS", "ТАБЛИЦА РЕКОРДОВ", "LEADERBOARD");
        LanguageData.Add("LOADING", "ЗАГРУЗКА...", "LOADING...");
        LanguageData.Add("HORNET", "ШЕРШЕНЬ", "HORNET");
        LanguageData.Add("BOOMSTICK", "ГРОМПАЛКА", "BOOMSTICK");
        LanguageData.Add("BULLSEYE", "ВЯБЛОЧКА", "BULLSEYE");
        LanguageData.Add("EQUILIZER", "УРАВНИТЕЛЬ", "EQUILIZER");
        LanguageData.Add("CRICKET", "СВЕРЧОК", "CRICKET");
        LanguageData.Add("KILL_START0", "УНИЧТОЖЬ ", "ELIMINATE ");
        LanguageData.Add("KILL_START1", " ПРОТИВНИКОВ\nУРОВЕНЬ ", " ENEMIES\nLEVEL ");
        LanguageData.Add("TIME_START0", "ПРЕВОЗМОГАЙ ", "OVERCOME FOR ");
        LanguageData.Add("TIME_START1", " СЕК\nУРОВЕНЬ ", " SEC\nLEVEL ");
        LanguageData.Add("SURVIVE_START", "ВЫЖИВАЙ\nТАК ДОЛГО,\nКАК СМОЖЕШЬ", "SURVIVE\nAS LONG\nAS YOUT CAN");
        LanguageData.Add("TIME_BONUS", "БОНУС\nЗА ВРЕМЯ", "TIME\nBONUS");
        LanguageData.Add("RESULTS", "ВАШ РЕЗУЛЬТАТ", "YOUR RESULT");
        LanguageData.Add("SETTINGS", "НАСТРОЙКИ", "SETTINGS");
        LanguageData.Add("PAUSE", "ПАУЗА", "PAUSE");
        LanguageData.Add("C_TYPE", "ТИП УПРАВЛЕНИЯ", "CONTROL TYPE");
        LanguageData.Add("C_SIDE", "СТОРОНА УПРАВЛЕНИЯ", "CONTROL SIDE");
        LanguageData.Add("SWIPE", "СВАЙП", "SWIPE");
        LanguageData.Add("STICK", "СТИК", "STICK");
        LanguageData.Add("CONNECTION", "СОЕДИНЕНИЕ С СЕРВЕРОМ,\nЖДИТЕ...", "SERVER CONNECTION\nPLEASE WAIT...");
        LanguageData.Add("NO_CONNECTION", "НЕ УДАЛОСЬ СОЕДИНИТЬСЯ\nС СЕРВЕРОМ", "SERVER CONNECION\nFAILED");
        LanguageData.Add("SHARE", "ПОДЕЛИТЬСЯ", "SHARE");
        LanguageData.Add("NAME_PLACEHOLDER", "ВВЕДИТЕ СВОЁ ИМЯ...", "ENTER YOUR NAME...");

        // создание словаря
        foreach (LanguageData ld in languageData) {
            dictionary.Add(ld.key, ld.value);
        }
        languageData.Clear();
        languageData = null;
    }

    // извлечь значение
    public static string GetValueForKey(string key) {

        return dictionary[key];
    }

    // язык
    public static bool isEnglish() {
        return english;
    }
}
