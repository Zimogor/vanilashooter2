﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// воспроизведение звуков
public class SoundsScript : MonoBehaviour {

    // публичные переменые
    public AudioSource audioSource; // звуки
    public AudioClip purchaseClip; // звук покупки
    public AudioClip equipClip; // звук экипировки оружия
    public AudioClip[] pageTurnClips; // звуки переварачивания страниц
    public AudioClip impactClip; // звук попадания во врага
    public AudioClip freezingClip; // звук замерзания
    public AudioClip gameOverLose; // звук поражения
    public AudioClip gameOverWin; // звук победы
    public AudioClip[] headPopClips; // звуки отрывающихся голов
    public AudioClip[] crackClips; // звуки трещин стекла
    public AudioClip[] screamClips; // звуки криков
    public AudioClip[] laughClips; // звуки смеха
    public AudioClip buttonClickClip; // стандартный звук кнопки

    // личные переменные
    private static SoundsScript instance; // ссылка на себя
    private Dictionary<string, AudioClip> dictionary; // словарь звуков

    // названия звуков
    public const string PURCHASE = "purchase"; // покупка оружия
    public const string EQUIP = "equip"; // экипировка оружия
    public const string PAGETURN = "pageturn"; // звуки листания
    public const string IMPACT = "impact"; // звук попадания во врага
    public const string FREEZING = "freezing"; // звук замерзания
    public const string GAMEOVERLOSE = "gameoverlose"; // звук поражения
    public const string GAMEOVERWIN = "gameoverwin"; // звук победы
    public const string HEADPOP = "headpop"; // звук отрывающейся головы
    public const string CRACK = "crack"; // звук трещины стекла
    public const string SCREAM = "scream"; // звуки криков зверей
    public const string LAUGH = "laugh"; // звуки смеха
    public const string BUTTON_CLICK = "buttonclick"; // стандартный звук кнопки

    // инициалиация
    private void Awake() {

        instance = this;
        dictionary = new Dictionary<string, AudioClip>();
        dictionary.Add(PURCHASE, purchaseClip);
        dictionary.Add(EQUIP, equipClip);
        dictionary.Add(PAGETURN + "0", pageTurnClips[0]);
        dictionary.Add(PAGETURN + "1", pageTurnClips[1]);
        dictionary.Add(IMPACT, impactClip);
        dictionary.Add(FREEZING, freezingClip);
        dictionary.Add(GAMEOVERLOSE, gameOverLose);
        dictionary.Add(GAMEOVERWIN, gameOverWin);
        for (int i = 0; i < 3; i++)
            dictionary.Add(HEADPOP + i.ToString(), headPopClips[i]);
        for (int i = 0; i < 4; i++)
            dictionary.Add(CRACK + i.ToString(), crackClips[i]);
        for (int i = 0; i < 5; i++)
            dictionary.Add(SCREAM + i.ToString(), screamClips[i]);
        for (int i = 0; i < laughClips.Length; i++) {
            dictionary.Add(LAUGH + i.ToString(), laughClips[i]);
        }
        dictionary.Add(BUTTON_CLICK, buttonClickClip);
    }

    // деструктор
    private void OnDestroy() {

        instance = null;
    }

    // -------------------------
    // проигрывание звуков
    // -------------------------

    // звук по названию
    public static void PlaySound(string dictKey) {

        // рандомность
        switch (dictKey) {
            case PAGETURN:
                dictKey = PAGETURN + Random.Range(0, 2).ToString();
                break;
            case HEADPOP:                
                dictKey = HEADPOP + Random.Range(0, 3).ToString();
                break;
            case CRACK:
                dictKey = CRACK + Random.Range(0, 4).ToString();
                break;
            case SCREAM:
                dictKey = SCREAM + Random.Range(0, 5).ToString();
                break;
            case LAUGH:
                dictKey = LAUGH + Random.Range(0, instance.laughClips.Length).ToString();
                break;
        }
        
        // проиграть звук
        instance.audioSource.PlayOneShot(instance.dictionary[dictKey]);
    }

    // звук по готовому клипу
    public static void PlaySound(AudioClip clip) {

        // проиграть звук
        instance.audioSource.PlayOneShot(clip);
    }
}
