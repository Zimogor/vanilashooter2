﻿using UnityEngine;
using System.Collections;

// идентификаторы сцен
public enum SceneID { NONE = -1, START_MENU = 0, MAIN_MENU = 1, LEVEL = 2 }
public enum LevelSurrounding { SUMMER, WINTER, FOREST } // окружение уровня
public enum LevelWinType { KILLING, TIMING, SURVIVING } // тип победы (количество убитых, время выживания, простая игра)

// управление загрузкой уровней
public class Loader : MonoBehaviour {

    // данные для загрузки уровня
    public static LevelData levelData;
    public class LevelData {

        // строитель данных уровня
        public class Builder {

            // переменные и константы
            public LevelSurrounding levelSurrounding; // тип уровня
            public LevelWinType levelWinType; // тип победы
            public int winCondition; // условие победы
            public int winPrize; // вознаграждение победы
            public int weaponNumber; // номер оружия
            public int difficulty; // сложность уровня

            private bool winParamsFlag = false;
            private bool diffParamsFlag = false;

            // конструктор
            public Builder(LevelSurrounding levelSurrounding, LevelWinType levelWinType) {
                this.levelSurrounding = levelSurrounding;
                this.levelWinType = levelWinType;
            }
            public Builder SetWinParams(int winCondition, int winPrize) {
                this.winCondition = winCondition;
                this.winPrize = winPrize;
                winParamsFlag = true;
                return this;
            }
            public Builder SetDiffParams(int weaponNumber, int difficulty) {
                this.weaponNumber = weaponNumber;
                this.difficulty = difficulty;
                diffParamsFlag = true;
                return this;
            }

            // качество
            public bool CheckQuality() {
                return diffParamsFlag && winParamsFlag;
            }
        }

        // конструктор
        public LevelData(Builder builder) {

            if (!builder.CheckQuality()) Debug.LogError("low quality level builder");
            levelSurrounding = builder.levelSurrounding;
            levelWinType = builder.levelWinType;
            winCondition = builder.winCondition;
            winPrize = builder.winPrize;
            weaponNumber = builder.weaponNumber;
            difficulty = builder.difficulty;
        }

        // переменные и константы
        public LevelSurrounding levelSurrounding { get; private set; } // тип уровня
        public LevelWinType levelWinType { get; private set; } // тип победы
        public int winCondition { get; private set; } // условие победы
        public int winPrize { get; private set; } // вознаграждение победы
        public int weaponNumber { get; private set; } // номер оружия
        public int difficulty { get; private set; } // сложность уровня
        // вывести в консоль
        public string GetDescription() {
            return "levelSurrounding: " + levelSurrounding + ", levelWinType: " + levelWinType + ", winCondition: " + winCondition + ", winPrize: " + winPrize + ", weaponNumber " + weaponNumber + ", difficulty " + difficulty;
        }
    }

    // личные переменные
    public static SceneID prevLevel { get; private set; } // предыдущий уровень
    public static SceneID curLevel { get; private set; } // текущий уровень

    // статический конструктор
    static Loader() {

        prevLevel = SceneID.NONE;
        curLevel = (SceneID)Application.loadedLevel;

        // создать загрузку для уровня по умолчанию (используется для отладки)
        LevelData.Builder builder = new LevelData.Builder(LevelSurrounding.SUMMER, LevelWinType.SURVIVING);
        builder.SetWinParams(0, 0);
        builder.SetDiffParams(0, 5);

        levelData = new LevelData(builder);
    }

    // перезапустить уровень
    public static void LoadLevelFromLevel(LevelData levelData) {

        Loader.levelData = levelData;
        Application.LoadLevel((int)SceneID.LEVEL);
        prevLevel = SceneID.LEVEL;
        curLevel = SceneID.LEVEL;
    }

    // загрузить уровень
    public static void LoadLevelFromMenu(LevelData levelData) {

        Loader.levelData = levelData;
        Application.LoadLevel((int)SceneID.LEVEL);
        prevLevel = SceneID.MAIN_MENU;
        curLevel = SceneID.LEVEL;
    }

    // загрузить стартовый экран
    public static void LoadStartFromMenu() {

        Application.LoadLevel((int)SceneID.START_MENU);
        prevLevel = SceneID.MAIN_MENU;
        curLevel = SceneID.START_MENU;
    }

    // загрузить меню
    public static void LoadMenuFromStart() {

        Application.LoadLevel((int)SceneID.MAIN_MENU);
        prevLevel = SceneID.START_MENU;
        curLevel = SceneID.MAIN_MENU;
    }

    // загрузить меню
    public static void LoadMenuFromLevel() {

        Application.LoadLevel((int)SceneID.MAIN_MENU);
        prevLevel = SceneID.LEVEL;
        curLevel = SceneID.MAIN_MENU;
    }
}
