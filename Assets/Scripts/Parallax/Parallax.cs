﻿using UnityEngine;
using System.Collections;

// вычислитель смещения параллакса
public class Parallax : MonoBehaviour {

    // публичные переменные
    public static float x = 0.0f; // значение параллакса X
    public static float y = 0.0f; // значение параллакса Y

#if UNITY_ANDROID

    // личные переменные
    private static bool gyrEnabled = false; // возможность использовать гироскоп
    private static bool gyrInited = false; // гироскоп инициализирован

    // определение возможности использовать гироскоп
    void Awake() {

        if (!Parallax.gyrInited) {
            Input.gyro.enabled = true;
            Parallax.gyrEnabled = Input.gyro.enabled;
        }
        Parallax.gyrInited = true;
    }

    // обновление
    private void Update() {

        // для андроида
        targetX = 0.0f;
        targetY = 0.0f;
        if (gyrEnabled) {

            // работа с гироскопом
            targetY = Input.gyro.rotationRateUnbiased.x * Screen.width * 0.5f;
            targetX = Input.gyro.rotationRateUnbiased.y * Screen.width * 0.5f;
        }
        else {

            // работа с акселерометром
            targetX = Input.acceleration.x * Screen.width;
            targetY = (Mathf.Abs(Input.acceleration.y) - 0.6f) * Screen.width;
        }

    }

    // фиксированное обновление
    float targetX = 0.0f;
    float targetY = 0.0f;
    private void FixedUpdate() {

        // вычисление параллакса
        float velocityX = 0.0f;
        Parallax.x = Mathf.SmoothDamp(Parallax.x, targetX, ref velocityX, 0.1f);
        float velocityY = 0.0f;
        Parallax.y = Mathf.SmoothDamp(Parallax.y, -targetY, ref velocityY, 0.1f);
    }

#else

    // обновление для PC
    private void Update() {

        // для PC
        Parallax.x = Input.mousePosition.x - Screen.width * 0.5f;
        Parallax.y = Input.mousePosition.y - Screen.height * 0.5f;
    }

#endif

}
