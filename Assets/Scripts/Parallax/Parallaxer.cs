﻿using UnityEngine;
using System.Collections;

// движение по параллаксу
public class Parallaxer : MonoBehaviour {

    // публичные переменные
    public float amplitude = 0.1f; // амплитуда смещения в долях экрана

    // личные переменные
    private Vector2 basePosition; // базовая координата
    private RectTransform rect; // транформация

    // пробуждение
    private void Awake() {

        rect = (RectTransform)transform;
        basePosition = rect.anchoredPosition;
    }

    // обновление
    private void LateUpdate() {

        float x = basePosition.x - Parallax.x * amplitude;
        float y = basePosition.y - Parallax.y * amplitude;
        rect.anchoredPosition = new Vector2(x, y);
    }
}
