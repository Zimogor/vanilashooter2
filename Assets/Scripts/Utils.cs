﻿using UnityEngine;
using System.Collections;

// вспомогательные функции
public class Utils : MonoBehaviour {

    // определить премию для уровня сложности
    public static int GetPrizeForLevel(int level) {

        return 100 + level * 50;
    }

    // определить условие победы для уровня сложности и типа уровня
    public static int GetConditionForLevel(int level, LevelWinType winType) {

        switch(winType) {

            case LevelWinType.KILLING:
                return Constants.FAST_GAME_WIN ? 1 : 15 + level * 2;
            case LevelWinType.TIMING:
                return Constants.FAST_GAME_WIN ? 10 : 30 + level * 5;
        }

        return -1;
    }

    // вычислить предполагаемый уровень исходя из скорости камеры
    public static int GetLevelFromCamVelocity(float camVelocity) {

        int result = (int)((camVelocity - 2.0f) / 0.1f);

        // проверка соответствия
        if (Mathf.Abs(camVelocity - GetCameraVelocityForLevel(result)) > 1.1f) {

            // проверка сломалась
            Debug.LogError("несоответствие функций зависимости скорости камеры и сложности");
        }
        return result;
    }

    // начальная скорость камеры в зависимости от сложности
    public static float GetCameraVelocityForLevel(int level) {

        return 2.0f + level * 0.1f;
    }

    // стартовая плотность врагов в зависимости от сложности
    public static float GetEnemyDensityForLevel(int level) {

        return 0.3f + level * 0.1f;
    }

    // стартовое зверство врагов в зависимости от сложности
    public static float GetEnemyAngerForLevel(int level) {

        return 0.0f + level * 0.05f;
    }

    // начальный бонус сложности в зависимости от сложности
    public static float GetSpeedBonusForLevel(int level) {

        return 1.0f + level * 0.2f;
    }
}
