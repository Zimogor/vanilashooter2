﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

// описание выбранного уровня
public class LevelDescription : MonoBehaviour {

    // публичные переменные
    public Text descriptionText; // тект описания
    public Text moneyText; // денежный приз
    public Text goalText; // количество цели
    public Image goalImage; // изображение цели
    public Sprite[] goalImageSprites; // набор спрайтов цели (0 - killing, 1 - time)

    // установить описание
    public void SetDescription(LevelWinType levelWinType, int difficulty, int money, int goal) {

        switch (levelWinType) {
            case LevelWinType.KILLING:

                // убей всех
                descriptionText.text = Localizator.GetValueForKey("KILL_LEVEL") +  ": " + difficulty;
                moneyText.text = money.ToString();
                goalText.text = goal.ToString();
                goalImage.enabled = true;
                goalImage.sprite = goalImageSprites[0];
                break;

            case LevelWinType.SURVIVING:

                // выживание
                descriptionText.text = Localizator.GetValueForKey("SURVIVE_LEVEL");
                moneyText.text = "+";
                goalText.text = "";
                goalImage.enabled = false;
                break;

            case LevelWinType.TIMING:

                // нет времени
                descriptionText.text = Localizator.GetValueForKey("TIME_LEVEL") + ": " + difficulty;
                moneyText.text = money.ToString();
                goalText.text = goal.ToString();
                goalImage.enabled = true;
                goalImage.sprite = goalImageSprites[1];
                break;
        }
    }
}
