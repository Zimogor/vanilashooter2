﻿using UnityEngine;
using System.Collections;

// неубиваемая музыка
public class UndestructableMusic : MonoBehaviour {

    // публичные переменные
    public AudioClip[] clips; // клипы для игры (0 - mainMenu, 1 и 2 - ingame)
    public AudioSource audioSource; // музыка
    public AudioSource startGameAudioSource; // звук кнопки "start"

    // личные переменные
    private static UndestructableMusic instance = null; // ссылка на себя
    private bool destroyed = true; // флаг уничтоженности
    private int levelMusic = 1; // индекс музыки уровня

    // пробуждение
    private void Awake() {

        // синглтон паттерн
        if (instance != null) Destroy(gameObject);
        else {
            destroyed = false;
            instance = this;
            audioSource.ignoreListenerVolume = true;
            DontDestroyOnLoad(gameObject);
        }
    }

    // старт
    private void Start() {

        if (!destroyed) {

            if (Loader.curLevel == SceneID.LEVEL) StartLevelMusicInNextFrame();
            else StartMenuMusicInNextFrame();
            audioSource.volume = SharedPrefs.musicOn ? 1.0f : 0.0f;
            AudioListener.volume = SharedPrefs.soundOn ? 1.0f : 0.0f;
        }
    }

    // загрузка уровня
    private void OnLevelWasLoaded(int level) {

        if (Loader.curLevel == SceneID.MAIN_MENU && Loader.prevLevel == SceneID.LEVEL) {

            // сменить музыку уровня на музыку меню
            StartMenuMusicInNextFrame();
        }
        else if (Loader.curLevel == SceneID.LEVEL) {

            // сменить музыку меню на музыку уровня
            StartLevelMusicInNextFrame();
        }
    }

    // включить музыку меню в следующем кадре
    private void StartMenuMusicInNextFrame() { StartCoroutine(StartMenuMusicInNextFrameCor()); }
    private IEnumerator StartMenuMusicInNextFrameCor() {

        yield return 0;

        audioSource.Stop();
        audioSource.clip = clips[0];
        audioSource.Play();
    }

    // включить музыку уровня в следующем кадре
    private void StartLevelMusicInNextFrame() { StartCoroutine(StartLevelMusicInNextFrameCor()); }
    private IEnumerator StartLevelMusicInNextFrameCor() {

        yield return 0;

        levelMusic = levelMusic == 1 ? 2 : 1;
        audioSource.Stop();
        audioSource.clip = clips[levelMusic];
        if (!GameController.paused) audioSource.Play();
    }

    // ------------------------
    // getters and setters
    // ------------------------

    // управление музыкой
    public static void MusicOn(bool musicOn) {

        instance.audioSource.volume = musicOn ? 1.0f : 0.0f;
    }

    // пауза и восстановление
    public static void SetPause(bool pause) {

        if (pause) {
            if (instance.audioSource.isPlaying) instance.audioSource.Pause();
        }
        else {
            if (!instance.audioSource.isPlaying) instance.audioSource.Play();
        }
    }

    // звук начала игры
    public static void StartGameSound() {

        instance.startGameAudioSource.Play();
    }
}
