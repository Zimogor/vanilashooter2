﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using System;

// сохранение данных на диск
public class SharedPrefs : MonoBehaviour {

    // текущий прогресс игры
    public static CurProgress curProgress;
    public class CurProgress {

        // текущая сложность
        public int timingLevel;
        public int killingLevel;
        public int survivingLevel;

        // текущее положение уровня
        public LevelWinType pos0; // forest
        public LevelWinType pos1; // summer
        public LevelWinType pos2; // winter

        // определить уровень сложности для типа победы
        public int GetDifficultyByWinType(LevelWinType winType) {

            switch (winType) {
                case LevelWinType.KILLING:
                    return killingLevel;
                case LevelWinType.SURVIVING:
                    return survivingLevel;
                case LevelWinType.TIMING:
                    return timingLevel;
            }
            Debug.LogError("загадочная ошибка");
            return -1;
        }

        // установить уровень сложности для типа победы
        public void SetDifficultyForWinType(LevelWinType winType, int difficulty) {

            switch (winType) {
                case LevelWinType.KILLING:
                    killingLevel = difficulty;
                    break;
                case LevelWinType.SURVIVING:
                    survivingLevel = difficulty;
                    break;
                case LevelWinType.TIMING:
                    timingLevel = difficulty;
                    break;
            }
        }

        // перемешать уровни (сменить окружение для указанного типа)
        public void MixLevels(LevelWinType forceChangeSurrounding) {

            // перемешать как попало
            if (forceChangeSurrounding == LevelWinType.SURVIVING) {
                SetLevelsByLevelIndex(UnityEngine.Random.Range(0, 6));
                return;
            }

            // перемешать с принуждением смены окружения
            List<int> levelIndices = new List<int>() { 0, 1, 2, 3, 4, 5 };
            LevelSurrounding prevSurrounding = GetSurroundingByWinType(forceChangeSurrounding);
            do {                    
                int arrayIndex = UnityEngine.Random.Range(0, levelIndices.Count);
                int randomIndex = levelIndices[arrayIndex];
                levelIndices.RemoveAt(arrayIndex);
                SetLevelsByLevelIndex(randomIndex);
            } while (prevSurrounding == GetSurroundingByWinType(forceChangeSurrounding));
        }

        // установить уровни по индексу
        public void SetLevelsByLevelIndex(int levelMixIndex) {

            switch (levelMixIndex) {
                case 0:
                    curProgress.pos0 = LevelWinType.TIMING;
                    curProgress.pos1 = LevelWinType.SURVIVING;
                    curProgress.pos2 = LevelWinType.KILLING;
                    break;
                case 1:
                    curProgress.pos0 = LevelWinType.TIMING;
                    curProgress.pos1 = LevelWinType.KILLING;
                    curProgress.pos2 = LevelWinType.SURVIVING;
                    break;
                case 2:
                    curProgress.pos0 = LevelWinType.SURVIVING;
                    curProgress.pos1 = LevelWinType.TIMING;
                    curProgress.pos2 = LevelWinType.KILLING;
                    break;
                case 3:
                    curProgress.pos0 = LevelWinType.SURVIVING;
                    curProgress.pos1 = LevelWinType.KILLING;
                    curProgress.pos2 = LevelWinType.TIMING;
                    break;
                case 4:
                    curProgress.pos0 = LevelWinType.KILLING;
                    curProgress.pos1 = LevelWinType.SURVIVING;
                    curProgress.pos2 = LevelWinType.TIMING;
                    break;
                case 5:
                    curProgress.pos0 = LevelWinType.KILLING;
                    curProgress.pos1 = LevelWinType.TIMING;
                    curProgress.pos2 = LevelWinType.SURVIVING;
                    break;
            }
        }

        // получить индекс уровней
        public int GetLevelIndex() {

            if (curProgress.pos0 == LevelWinType.TIMING && curProgress.pos1 == LevelWinType.KILLING && curProgress.pos2 == LevelWinType.SURVIVING) return 1;
            else if (curProgress.pos0 == LevelWinType.SURVIVING && curProgress.pos1 == LevelWinType.TIMING && curProgress.pos2 == LevelWinType.KILLING) return 2;
            else if (curProgress.pos0 == LevelWinType.SURVIVING && curProgress.pos1 == LevelWinType.KILLING && curProgress.pos2 == LevelWinType.TIMING) return 3;
            else if (curProgress.pos0 == LevelWinType.KILLING && curProgress.pos1 == LevelWinType.SURVIVING && curProgress.pos2 == LevelWinType.TIMING) return 4;
            else if (curProgress.pos0 == LevelWinType.KILLING && curProgress.pos1 == LevelWinType.TIMING && curProgress.pos2 == LevelWinType.SURVIVING) return 5;
            return 0;
        }

        // получить тип местности по типу победы
        public LevelSurrounding GetSurroundingByWinType(LevelWinType winType) {

            if (pos0 == winType) return LevelSurrounding.FOREST;
            if (pos1 == winType) return LevelSurrounding.SUMMER;
            if (pos2 == winType) return LevelSurrounding.WINTER;

            // ошибка
            Debug.LogError("неизвестная ошибка");
            return LevelSurrounding.FOREST;
        }
    }

    // личные переменные
    private static int score; // очки
    private static int money; // деньги
    private static bool firstPlayerGame; // первый старт уровня
    public static bool stickControltype { get; private set; } // управление стиком
    public static bool moveLeftShootRight { get; private set; } // тип управления
    public static bool[] weaponSold { get; private set; } // флаги проданности оружия
    public static bool musicOn { get; private set; } // включенность музыки
    public static bool soundOn { get; private set; } // включенность звуков
    public static int lastSavedScore { get; private set; } // последнее сохранённое количество очков
    private static string leaderBoardID = null; // идентификатор в таблице рекордов (ленивый)
    private const string SCORE_KEY = "score"; // ключ для очков
    private const string MONEY_KEY = "money"; // ключ для денег
    private const string FIRST_START_KEY = "firststart"; // ключ для первого старта
    private const string SIDE_CONTROL_TYPE = "sideControlType"; // сторона управления
    private const string STICK_CONTROL_TYPE = "stickControlType"; // управление стиком
    private const string TIMING_LEVEL_KEY = "timingLevel"; // уровень уровня победы временем
    private const string KILLING_LEVEL_KEY = "killingLevel"; // уровень уровня победы убийствами
    private const string SURVIVING_LEVEL_KEY = "survivingLevel"; // сложность уровня выживания
    private const string LEVEL_ORDERING_KEY = "levelOrdering"; // последовательность уровней
    private const string SOUND_ON_KEY = "soundOn"; // включенность звуков
    private const string MUSIC_ON_KEY = "musicOn"; // включенность звуков
    private const string LEADERBOARD_ID = "leaderboardID"; // идентификатор в таблице рекордов
    private const string LAST_SAVED_SCORE = "lastSavedScore"; // последнее сохранённое количество очков

    // данная последовательность используется в магазине и в порождении оружии при старте уровня
    private static string[] WEAPON_KEYS = new string[] {"w_hornet", "w_bullseye", "w_boomstick", "w_equilizer", "w_cricket" }; // ключи для проданности оружия

    // статический конструктор
    static SharedPrefs() {

        // загрузить разные данные с диска
        LoadDataFromDisc();
    }

    // загрузить данные с диска
    private static void LoadDataFromDisc() {

        // очистить всё
        if (Constants.CLEAR_PREFS_ON_START) PlayerPrefs.DeleteAll();

        // загрузить разные данные с диска
        score = PlayerPrefs.GetInt(SCORE_KEY, 0);
        money = PlayerPrefs.GetInt(MONEY_KEY, 0);
        moveLeftShootRight = (PlayerPrefs.GetInt(SIDE_CONTROL_TYPE, 0) == 1);
        stickControltype = (PlayerPrefs.GetInt(STICK_CONTROL_TYPE, 0) == 1);
        firstPlayerGame = (PlayerPrefs.GetInt(FIRST_START_KEY, 1) != 0);
        musicOn = (PlayerPrefs.GetInt(MUSIC_ON_KEY, 1) == 1);
        soundOn = (PlayerPrefs.GetInt(SOUND_ON_KEY, 1) == 1);
        lastSavedScore = PlayerPrefs.GetInt(LAST_SAVED_SCORE, 0);

        // загрузить проданность оружий с диска
        weaponSold = new bool[WEAPON_KEYS.Length];
        weaponSold[0] = true;
        for (int i = 1; i < weaponSold.Length; i++) {
            weaponSold[i] = PlayerPrefs.GetInt(WEAPON_KEYS[i], 0) == 1;
        }

        // загрузить прогресс игры
        curProgress = new CurProgress();
        curProgress.timingLevel = PlayerPrefs.GetInt(TIMING_LEVEL_KEY, 1);
        curProgress.killingLevel = PlayerPrefs.GetInt(KILLING_LEVEL_KEY, 1);
        curProgress.survivingLevel = PlayerPrefs.GetInt(SURVIVING_LEVEL_KEY, 0);
        int levelMixIndex = PlayerPrefs.GetInt(LEVEL_ORDERING_KEY, 0);
        curProgress.SetLevelsByLevelIndex(levelMixIndex);
    }

    // сохранить громкость звуков
    public static void SaveSoundOn(bool soundOn) {

        SharedPrefs.soundOn = soundOn;
        PlayerPrefs.SetInt(SOUND_ON_KEY, soundOn ? 1 : 0);
        PlayerPrefs.Save();
    }

    // сохранить громкость музыки
    public static void SaveMusicOn(bool musicOn) {

        SharedPrefs.musicOn = musicOn;
        PlayerPrefs.SetInt(MUSIC_ON_KEY, musicOn ? 1 : 0);
        PlayerPrefs.Save();
    }

    // сохранить прогресс игры
    public static void SaveGameProgress() {

        PlayerPrefs.SetInt(TIMING_LEVEL_KEY, curProgress.timingLevel);
        PlayerPrefs.SetInt(KILLING_LEVEL_KEY, curProgress.killingLevel);
        PlayerPrefs.SetInt(LEVEL_ORDERING_KEY, curProgress.GetLevelIndex());
        PlayerPrefs.SetInt(SURVIVING_LEVEL_KEY, curProgress.survivingLevel);
        PlayerPrefs.Save();
    }

    // сохранить проданность оружия
    public static void WeaponSoldAndSave(int number) {

        weaponSold[number] = true;
        PlayerPrefs.SetInt(WEAPON_KEYS[number], 1);
        PlayerPrefs.Save();
    }

    // сохранить сторону управления
    public static void SaveControlSideType(bool moveLeftShootRight) {

        SharedPrefs.moveLeftShootRight = moveLeftShootRight;
        PlayerPrefs.SetInt(SIDE_CONTROL_TYPE, moveLeftShootRight ? 1 : 0);
        PlayerPrefs.Save();
    }

    // сохранить тип управления
    public static void SaveControlType(bool stickControlType) {

        SharedPrefs.stickControltype = stickControlType;
        PlayerPrefs.SetInt(STICK_CONTROL_TYPE, stickControltype ? 1 : 0);
        PlayerPrefs.Save();
    }

    // добавить очки и деньги
    public static void AppendScoreAndSave(int score, int money) {

        SaveScoreAndMoney(SharedPrefs.score + score, SharedPrefs.money + money);
    }

    // сохранить очки и деньги
    public static void SaveScoreAndMoney(int score, int money) {

        SharedPrefs.score = score;
        SharedPrefs.money = money;

        // сохранить на диск
        PlayerPrefs.SetInt(SCORE_KEY, score);
        PlayerPrefs.SetInt(MONEY_KEY, money);
        PlayerPrefs.Save();
    }

    // сохранить деньги
    public static void SaveMoney(int money) {

        SharedPrefs.money = money;
        PlayerPrefs.SetInt(MONEY_KEY, money);
        PlayerPrefs.Save();
    }

    // сохранить очки
    public static void SaveScore(int score) {

        SharedPrefs.score = score;
        PlayerPrefs.SetInt(SCORE_KEY, score);
        PlayerPrefs.Save();
    }

    // сохранить последнее сохранённое количество очков на сервере
    public static void SaveLastSavedScore(int score) {

        SharedPrefs.lastSavedScore = score;
        PlayerPrefs.SetInt(LAST_SAVED_SCORE, score);
        PlayerPrefs.Save();
    }

    // загрузить очки и деньги
    public static void GetScore(out int score, out int money) {

        score = SharedPrefs.score;
        money = SharedPrefs.money;
    }

    // самый первый запуск уровня
    public static bool isFirstPlayerGame() {

        return firstPlayerGame;
    }

    // запуск больше не первый
    public static void FirstPlayerGameNoMore() {

        if (firstPlayerGame) {

            PlayerPrefs.SetInt(FIRST_START_KEY, 0);
            PlayerPrefs.Save();
        }
        firstPlayerGame = false;
    }

    // очистить все
    public static void ClearAll() {

        PlayerPrefs.DeleteAll();
        LoadDataFromDisc();
    }

    // идентификатор таблицы рекордов
    public static string GetLeaderBoardID() {        

        if (leaderBoardID == null) {

            if (PlayerPrefs.HasKey(LEADERBOARD_ID)) {

                // идентификатор есть
                leaderBoardID = PlayerPrefs.GetString(LEADERBOARD_ID);
            }
            else {

                // идентификатора нет, создать
                byte[] b = new byte[8];
                System.Random rnd = new System.Random();
                rnd.NextBytes(b);
                SHA1Managed sha1 = new SHA1Managed();
                var hash = sha1.ComputeHash(b);
                leaderBoardID = Convert.ToBase64String(hash);
                PlayerPrefs.SetString(LEADERBOARD_ID, leaderBoardID);
                PlayerPrefs.Save();
            }
        }

        return leaderBoardID;
    }
}
