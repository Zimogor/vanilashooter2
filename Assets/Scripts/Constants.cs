﻿using UnityEngine;
using System.Collections;

// перечисление направления
public enum HorDirection { RIGHT, LEFT };

// управляющие константы
public class Constants : MonoBehaviour {

    // везде правильно false
    public static bool CLEAR_PREFS_ON_START = false; // чистить всё при старте
    public static bool CHEAT_BUTTONS = false; // читерские кнопки (выкачивание денег, выкачивание очков, сброс настроек) // todo
    public static bool SETTINGS_PC_ON = false; // включить настройки для PC
    public static bool MIN_LIFE_AMOUNT = false; // количество жизней у героя - 1
    public static bool FAST_GAME_WIN = false; // быстрая победа
    public static bool OFF_SITELOCK_FGL = false; // отключить сайтлок на FGL
}
