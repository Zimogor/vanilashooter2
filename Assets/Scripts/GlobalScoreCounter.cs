﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

// глобальный счётчик очков
public class GlobalScoreCounter : MonoBehaviour {

    // публичные переменные
    public Text scoreText; // текст с очками
    public Text moneyText; // текст с деньгами

    // личные переменные
    private static int score; // счёт очков
    private static int money; // счёт денег
    private static GlobalScoreCounter instance; // ссылка на себя

    // статический конструктор
    static GlobalScoreCounter() {

        score = 0;
        money = 0;
    }

    // пробуждение
    private void Awake() {

        instance = this;

        // загрузить деньги и очки
        SharedPrefs.GetScore(out score, out money);
        scoreText.text = score.ToString();
        moneyText.text = money.ToString();
    }

    // деструктор
    private void OnDestroy() {

        instance = null;
    }

    // получить количество денег
    public static int GetMoney() {

        return money;
    }

    // получить очки
    public static int GetScore() {

        return score;
    }

    // вычесть денег (возвращает, сколько осталось)
    public static int SubstractMoneyAndSave(int price) {

        money -= price;
        instance.moneyText.text = money.ToString();
        SharedPrefs.SaveMoney(money);
        return money;
    }

    // добавить денег (возвращает, сколько осталось)
    public static int AddMoneyAndSave(int killing) {

        money += killing;
        instance.moneyText.text = money.ToString();
        SharedPrefs.SaveMoney(money);
        return money;
    }

    // добавить очки (возвращает, сколько осталось)
    public static int AddScoreAndSave(int killing) {

        score += killing;
        instance.scoreText.text = score.ToString();
        SharedPrefs.SaveScore(score);
        return score;
    }
}
