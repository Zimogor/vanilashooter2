﻿using UnityEngine;
using System.Collections;

// двигатель камеры
public class CameraMover : MonoBehaviour {

    // публичные переменные
    public float acceleration = 0.02f; // нарастание скорости (в секунду)

    // личные переменные
    private float velocity; // скорость движения камеры    
    private float leftVisibleBorder, rightVisibleBorder, topVisibleBorder, bottomVisibleBorder; // видимые границы камеры
    private float halfCamWidth; // половина ширины камеры
    private float camWidth, camHeight; // ширина и высота камеры
    private Camera cam; // непосредственно камера
    private Animator anim; // аниматор

    // пробуждение
    private void Awake() {

        // указатели
        cam = gameObject.GetComponent<Camera>();
        anim = gameObject.transform.parent.gameObject.GetComponent<Animator>();

        // разное
        velocity = Utils.GetCameraVelocityForLevel(Loader.levelData.difficulty);
        float halfCamHeight = cam.orthographicSize;
        halfCamWidth = halfCamHeight * cam.aspect;
        camWidth = halfCamWidth * 2.0f;
        camHeight = halfCamHeight * 2.0f;
        leftVisibleBorder = transform.position.x - halfCamWidth;
        rightVisibleBorder = transform.position.x + halfCamWidth;
        topVisibleBorder = transform.position.y + halfCamHeight;
        bottomVisibleBorder = topVisibleBorder - halfCamHeight * 2.0f;
    }

    // обновление
    private void Update() {
        if (GameController.paused) return;

        // двигать камеру
        transform.Translate(velocity * Time.deltaTime, 0, 0);

        // обновить видимые границы
        leftVisibleBorder = transform.position.x - halfCamWidth;
        rightVisibleBorder = transform.position.x + halfCamWidth;

        // ускорение
        velocity += Time.deltaTime * acceleration;
    }

    // ----------------------------
    // getters
    // ----------------------------

    // дрожание камеры
    public void Tremble() {

        anim.SetTrigger("tremble");
    }

    // преобразование координат
    public Vector3 ScreenToWorld(Vector3 screenPos) {

        return cam.ScreenToWorldPoint(screenPos);
    }

    // скорость
    public float GetVelocity() {

        return velocity;
    }

    // полная высота камеры
    public float GetHeight() {

        return camHeight;
    }

    // полная ширина камеры
    public float GetWidth() {

        return camWidth;
    }

    // левый видимый край
    public float GetLeftVisibleBorder() {

        return leftVisibleBorder;
    }

    // правый видимый край
    public float GetRightVisibleBorder() {

        return rightVisibleBorder;
    }

    // верхний видимый край
    public float GetTopVisibleBorder() {

        return topVisibleBorder;
    }

    // нижний видимый край
    public float GetBottomVisbleBorder() {

        return bottomVisibleBorder;
    }
}
