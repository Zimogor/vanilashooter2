﻿using UnityEngine;
using System.Collections;

// сборщик оставшехся предметов
public class GarbageCollector : MonoBehaviour {

    // старт
    private void Start() {

        Vector3 newPosition = transform.position;
        newPosition.x = SuperSingleton.cam.GetLeftVisibleBorder() - 3.0f;
        transform.position = newPosition;
    }

    // столкновение
    void OnTriggerEnter(Collider other) {
        
        if (other.gameObject.CompareTag("Item")) {

            GameObject bonus = other.gameObject.transform.parent.gameObject;
            Pooler.MakeChild(bonus);
            bonus.SetActive(false);
        }
    }
}
