﻿using UnityEngine;
using System.Collections;

// самоуничтожение объекта
public class SelfDestruct : MonoBehaviour {

    // публичные переменные
    public float lifeTime = 1.0f;
    public bool pooled = true;

    // старт
    private void Start() {

        if (!pooled) Destroy(gameObject, lifeTime);
    }

    // активация
    private void OnEnable() {

        if (pooled) Invoke("PseudoDestroy", lifeTime);
    }

    // уничтожение
    private void PseudoDestroy() {

        gameObject.SetActive(false);
    }

    // деактивация
    private void OnDisable() {

        CancelInvoke();
    }
}
