﻿using UnityEngine;
using System.Collections;

// враг
public class Enemy : MonoBehaviour, Damagable {

    // публичные переменные
    public GameObject headCollider, bodyCollider; // коллайдеры для стрельбы
    public float enemySpriteDisp; // смещение врага по высоте
    public float maxLifeAmount = 100.0f; // максимальное количество жизней
    public float attackDistance = 5.0f; // растояние, на котором начинают атаковать
    public AudioClip[] attackClips; // звуки атаки

    // личные переменные
    private float curLifeAmount; // текущее количество жизней
    private Animator animator; // аниматор
    private bool impactSoundFlag = false; // флаг воспроизведения звука удара (антидробь)

    // старт 
    private void Start() {

        animator = gameObject.GetComponent<Animator>();
    }

    // активация
    private void OnEnable() {

        float height = enemySpriteDisp * transform.localScale.y;
        transform.Translate(0.0f, -height, 0.0f);
        curLifeAmount = maxLifeAmount;
        headCollider.SetActive(true);
        bodyCollider.SetActive(true);
    }

    // обновление
    private void Update() {

        // выход за критическую отметку (перестать стрелять)
        if (transform.position.x < SuperSingleton.cam.GetRightVisibleBorder() - BackgroundConstructor.MIN_ENEMY_EXISTS_DISTANCE) {

            animator.SetBool("Attack", false);
            return;
        }

        // вхождение в зону атаки
        if (transform.position.x <= SuperSingleton.cam.GetRightVisibleBorder() - attackDistance) {

            animator.SetBool("Attack", true);
        }

        // флаг звука
        impactSoundFlag = false;
    }

    // получение повреждения
    void Damagable.OnGotDamage(Collider2D collider, float damage, Vector3 worldPosition, bool explosion) {

        // попадание в голову
        bool headShot = collider.gameObject.CompareTag("HeadCollider");

        // взровался
        if (explosion) {

            if (!headShot) {
                Deactivate(collider.transform.position + new Vector3(0.0f, collider.offset.y * transform.localScale.y, 0.0f));
                animator.SetTrigger("Burn");
            }
            return;
        }

        // звук
        if (!impactSoundFlag) SoundsScript.PlaySound(SoundsScript.IMPACT);
        impactSoundFlag = true;

        // обычное попадание
        curLifeAmount -= headShot ? damage * 2.0f : damage;
        if (curLifeAmount <= 0.0f) {

            // деактивировать объект
            Deactivate(worldPosition);

            // анимация
            animator.SetTrigger(headShot ? "HeadShot" : "Die");

            // взорваться перьями
            GameObject feather = Pooler.GetInstance(Pooled.FEATHER, worldPosition, Quaternion.Euler(headShot ? 270.0f : 0.0f, 0.0f, 0.0f));
            feather.SetActive(true);

            // звук взрыва
            if (headShot) SoundsScript.PlaySound(SoundsScript.HEADPOP);
        }
    }

    // деактивировать объект
    private void Deactivate(Vector3 bonusPoint) {

        // убрать коллайдеры
        headCollider.SetActive(false);
        bodyCollider.SetActive(false);

        // добавить очков
        ScoreCounter.AddScore((int)(10 * GameController.totalMultBonus));
        ScoreCounter.AddKilling(1);
        SoundsScript.PlaySound(SoundsScript.SCREAM);

        // выбить бонус
        Pooled bonusType = PrefabsList.ChooseRandomItem();
        if (bonusType != Pooled.NONE) {
            GameObject bonus = Pooler.GetInstanceWithNativeRotation(bonusType, bonusPoint + new Vector3(0.0f, 0.0f, -1.0f));
            bonus.SetActive(true);
        }
    }

    // удар
    public void Attack() {

        int clipNumber = Random.Range(0, attackClips.Length);
        SoundsScript.PlaySound(attackClips[clipNumber]);
        Player.GetDamage(2.0f);
    }
}
