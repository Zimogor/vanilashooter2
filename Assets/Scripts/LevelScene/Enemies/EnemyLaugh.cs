﻿using UnityEngine;
using System.Collections;

// спорадический смех
public class EnemyLaugh : MonoBehaviour {

    // публичные переменные
    public float minPeriod = 3.0f; // минимальный период смеха
    public float maxPeriod = 5.0f; // максимальный пероод смеха

    // старт
    private void Start() {

        StartCoroutine(Laugh());
    }

    // корутина смеха
    private IEnumerator Laugh() {

        do {
            yield return new WaitForSeconds(Random.Range(minPeriod, maxPeriod));
            SoundsScript.PlaySound(SoundsScript.LAUGH);
        } while (true);
    }
}
