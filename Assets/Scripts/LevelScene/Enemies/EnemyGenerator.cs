﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// генератор врагов у объекта
public class EnemyGenerator : MonoBehaviour {

    // личные переменные
    private List<GameObject> enemyList; // список врагов

    // пробуждение
    private void Awake() {

        enemyList = new List<GameObject>();
    }

    // сгенерировать врагов
    public void GenerateSomeone(float probability, float anger) {

        // массив точек рождения
        int childCount = transform.childCount;
        List<Transform> spawnPoints = new List<Transform>(childCount);
        for (int i = 0; i < childCount; i++) {
            spawnPoints.Add(transform.GetChild(i));
        }

        // сгенерировать врагов
        int integer = (int)(spawnPoints.Count * probability);
        float remain = spawnPoints.Count * probability - integer;
        int spawnPointAmount = integer + (Random.value < remain ? 1 : 0);
        for (int i = 0; i < spawnPointAmount; i++) {

            // объект
            bool invert = Random.value < 0.5f;
            int spawnPointIndex = Random.Range(0, spawnPoints.Count);
            GameObject enemyObject = Pooler.GetInstance(PrefabsList.ChooseRandomEnemy(), spawnPoints[spawnPointIndex].position, Quaternion.identity);

            // параметры
            float scale = (8.0f - enemyObject.transform.position.y) * 0.05f + 0.4f; // масштаб врагов (высота камеры, коэфициент увеличения масштаба, верхний масштаб)
            enemyObject.transform.parent = transform;
            enemyObject.transform.localScale = new Vector3(invert ? scale : -scale, scale, scale);
            enemyObject.GetComponent<Enemy>().attackDistance = Mathf.Lerp(BackgroundConstructor.MIN_ENEMY_EXISTS_DISTANCE, BackgroundConstructor.MIN_ENEMY_KIND_DISTANCE, anger);

            // инициализация
            enemyObject.SetActive(true);
            enemyList.Add(enemyObject);
            spawnPoints.RemoveAt(spawnPointIndex);
        }
    }

    // выгенерировать врагов обратно
    public void DegenerateAllEnemies() {

        foreach (GameObject enemy in enemyList) {

            Pooler.MakeChild(enemy);
            enemy.SetActive(false);
        }
    }

}
