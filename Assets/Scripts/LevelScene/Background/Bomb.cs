﻿using UnityEngine;
using System.Collections;

// бомба
public class Bomb : MonoBehaviour, Damagable {

    // публичные переменные
    public float explosionRadius = 3.0f; // радиус взрыва

    // получение повреждения
    void Damagable.OnGotDamage(Collider2D collider, float damage, Vector3 worldPosition, bool explosion) {

        // родить взрыв
        GameObject explosionObject = Pooler.GetInstance(Pooled.EXPLOSION, transform.position, Quaternion.identity);
        explosionObject.SetActive(true);
        SuperSingleton.cam.Tremble();

        // убрать объект
        Pooler.MakeChild(gameObject);
        gameObject.SetActive(false);

        // убить всё живое на экране
        CameraMover cam = SuperSingleton.cam;
        Vector2 pointA = new Vector2(cam.GetLeftVisibleBorder(), cam.GetBottomVisbleBorder());
        Vector2 pointB = new Vector2(cam.GetRightVisibleBorder(), cam.GetTopVisibleBorder());
        Collider2D[] colliders = Physics2D.OverlapAreaAll(pointA, pointB);

        foreach (Collider2D c in colliders) {

            // попадание во что-то
            c.gameObject.transform.parent.GetComponent<Damagable>().OnGotDamage(c, 500.0f, transform.position, true);
        }
    }
}
