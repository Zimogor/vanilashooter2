﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// цепочка звеньев
public class Chain : MonoBehaviour {

    // публичные переменные
    public GameObject chainLink; // звено цепочки
    public float parallax; // параллакс

    // личные переменные
    private CameraMover cam; // ссылка на камеру
    private int chainAmount; // количество звеньев для полного перекрытия
    private List<GameObject> chainLinks; // список звеньев
    private float endPoint; // конечная точка конечного звена
    private float startPoint; // конечная точка первого звена
    private float chainWidth; // ширина звена

    // старт
    private void Start() {

        // проверка правильности ввода
        if (chainLink == null) {
            Debug.LogError("для создания цепочки необходимо инициализировать объекты цепочек");
            return;
        }

        // инициализация
        cam = SuperSingleton.cam;
        chainLinks = new List<GameObject>();

        // вычисление размера звена
        GameObject chainLinkObject = Instantiate(chainLink, new Vector3(cam.GetLeftVisibleBorder(), 0, 0), Quaternion.identity) as GameObject;
        chainLinkObject.transform.parent = gameObject.transform;
        chainLinks.Add(chainLinkObject);
        chainWidth = chainLinkObject.GetComponent<SpriteRenderer>().bounds.size.x;
        chainWidth -= 0.01f; // один пиксель внахлёст, чтобы швов не было

        endPoint = chainLinkObject.transform.position.x + chainWidth;
        startPoint = endPoint;

        // вычисление количества звеньев
        chainAmount = (int)(cam.GetWidth() / chainWidth) + 2;

        // создание дополнительных звеньев
        for (int i = 0; i < chainAmount - 1; i++) {

            GameObject newChainLinkObject = Instantiate(chainLink, new Vector3(endPoint, 0, 0), Quaternion.identity) as GameObject;
            endPoint += chainWidth;
            chainLinks.Add(newChainLinkObject);
            newChainLinkObject.transform.parent = gameObject.transform;
        }
    }

    // обновление
    private void Update() {
        if (GameController.paused) return;

        // движение параллакса
        float move = Time.deltaTime * parallax;
        foreach (GameObject link in chainLinks) {
            link.transform.Translate(move, 0, 0);
        }
        startPoint += move;
        endPoint += move;

        // перемещение отставших звеньев
        if (startPoint < cam.GetLeftVisibleBorder()) {

            // первое звено скрылось (переместить)
            GameObject chainToRemove = chainLinks[0];
            chainToRemove.transform.position = new Vector3(endPoint, 0, 0);
            chainLinks.RemoveAt(0);
            chainLinks.Add(chainToRemove);
            startPoint = chainLinks[1].transform.position.x;
            endPoint = startPoint + chainWidth * (chainAmount - 1);
        }
    }
}
