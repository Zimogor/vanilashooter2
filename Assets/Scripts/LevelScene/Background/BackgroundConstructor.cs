﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// подготовка заднего фона
public class BackgroundConstructor : MonoBehaviour {

    // опорная ширина игрового поля (до неё враги не исчезают, а после неё, перестают стрелять
    // используется для снижения превосходства владельцев коротких телефонов
    public static float MIN_ENEMY_EXISTS_DISTANCE = 10.0f;
    public static float MIN_ENEMY_KIND_DISTANCE = 1.0f; // всегда добрые на этом расстоянии

    // комплект для уровня
    [System.Serializable]
    public struct LevelSet {

        public GameObject background;
        public GameObject earth;
        public List<GameObject> objectsToGenerate;
    }

    // публичные переменные
    public LevelSet forestSet; // лестой комплект
    public LevelSet summerSet; // летний комплект
    public LevelSet winterSet; // зимний комплект
    public Chain backgroundChain; // задний фон
    public Chain earthChain; // земля
    public ObjectsGenerator objectsGenerator; // генератор объектов

    // старт
    private void Awake() {

        // загрузить данные
        switch (Loader.levelData.levelSurrounding) {

            case LevelSurrounding.FOREST:

                backgroundChain.chainLink = forestSet.background;
                earthChain.chainLink = forestSet.earth;
                objectsGenerator.objectsToGenerate = forestSet.objectsToGenerate;
                objectsGenerator.height = 1.5f;

                break;
            case LevelSurrounding.SUMMER:

                backgroundChain.chainLink = summerSet.background;
                earthChain.chainLink = summerSet.earth;
                objectsGenerator.objectsToGenerate = summerSet.objectsToGenerate;
                objectsGenerator.height = 2.5f;

                break;
            case LevelSurrounding.WINTER:

                backgroundChain.chainLink = winterSet.background;
                earthChain.chainLink = winterSet.earth;
                objectsGenerator.objectsToGenerate = winterSet.objectsToGenerate;
                objectsGenerator.height = 1.5f;

                break;
        }
    }
}
