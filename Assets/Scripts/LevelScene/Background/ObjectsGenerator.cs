﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// генаратор объектов
public class ObjectsGenerator : MonoBehaviour {

    // публичные переменные
    public List<GameObject> objectsToGenerate; // список объектов для генерации
    public float minBombTime = 2.0f; // чтобы бомбы не стояли часто
    public float maxBombTime = 10.0f; // чтобы бомбы не стояли часто
    public float objectDistance = 1.0f; // расстояние между объектами
    public float height; // высота объектов

    // личные переменные
    private float nextBombTime; // чтобы бомбы не стояли часто
    private float nextPoint; // следующая точка для генерации объекта
    private float additionalSpace; // дополнительное место для генерации
    private List<GameObject> content; // сгенерированные объекты
    private CameraMover cam; // ссылка на камеру

    // параметры сложности
    private float difDensity; // сложность плотности (от 0 до 1)
    private float difDensityVelocity = 0.005f; // (в секунду)
    private float difAnger; // сложность гнева (от 0 до 1)
    private float difAngerVelocity = 0.005f; // (в секунду)

    // старт
    private void Start() {        

        nextBombTime = Random.Range(minBombTime, maxBombTime);
        nextPoint = 0;
        additionalSpace = 5.0f;
        content = new List<GameObject>();
        cam = SuperSingleton.cam;
        difDensity = Utils.GetEnemyDensityForLevel(Loader.levelData.difficulty);
        difAnger = Utils.GetEnemyAngerForLevel(Loader.levelData.difficulty);

        // добавить объекты
        AddNewObjects(false);
    }

    // обновление
    private void Update() {

        // нарастание сложности
        difDensity += Time.deltaTime * difDensityVelocity;
        difAnger += Time.deltaTime * difAngerVelocity;
        difDensity = Mathf.Clamp01(difDensity);
        difAnger = Mathf.Clamp01(difAnger);

        // добавить объекты
        AddNewObjects(true);

        // удалить объекты
        DeleteOldObjects();
    }

    // добавить объекты, если надо
    private void AddNewObjects(bool generateEnemies) {

        // добавить объекты
        while (nextPoint <= cam.GetRightVisibleBorder() + additionalSpace) {

            // добавить объект
            int index = Random.Range(0, objectsToGenerate.Count);
            GameObject newObject = Instantiate(objectsToGenerate[index], new Vector3(nextPoint, height, 0), Quaternion.identity) as GameObject;
            newObject.transform.parent = gameObject.transform;
            content.Add(newObject);
            nextPoint += objectDistance;

            // сгенерировать у объекта врагов
            if (generateEnemies) newObject.GetComponent<EnemyGenerator>().GenerateSomeone(difDensity, difAnger);
        }
    }

    // фиксированное обновление
    private void FixedUpdate() {

        // бомба
        nextBombTime -= Time.fixedDeltaTime;
        if (nextBombTime <= 0.0f) {

            nextBombTime = Random.Range(minBombTime, maxBombTime);
            GameObject bombObject = Pooler.GetInstance(Pooled.BOMB, new Vector3(cam.GetRightVisibleBorder() + 2.0f, 1.5f, 0), Quaternion.identity);
            bombObject.SetActive(true);
        }
    }

    // удалить старые объекты
    private void DeleteOldObjects() {

        float outOfScreanDistance = cam.GetLeftVisibleBorder() - additionalSpace; // вышел за экран
        float doNotDeleteDistance = cam.GetRightVisibleBorder() - BackgroundConstructor.MIN_ENEMY_EXISTS_DISTANCE; // нельзя удалять на коротких телефонах слишком рано
        if (content[0].transform.position.x < Mathf.Min(outOfScreanDistance, doNotDeleteDistance)) {

            // чтобы не удалить объекты из пула, их нужно выгрузить
            content[0].GetComponent<EnemyGenerator>().DegenerateAllEnemies();

            Destroy(content[0]);
            content.RemoveAt(0);
        }
    }
}
