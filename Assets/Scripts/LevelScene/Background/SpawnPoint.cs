﻿using UnityEngine;
using System.Collections;

// точка рождения врага
public class SpawnPoint : MonoBehaviour {

    // рисование иконки
    void OnDrawGizmos() {
        Gizmos.DrawIcon(transform.position + new Vector3(0, 0.4f, 0), "enemy_place.png", true);
    }
}
