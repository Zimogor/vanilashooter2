﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// перечень возможных объектов
public enum Pooled { NONE = -1, BULLET = 0, FEATHER = 1, PINK_BEAR = 2, COALA = 3, GREEN_GUY = 4, COIN = 5, GOLD = 6, CHEST = 7, HEAL = 8, FREEZE = 9, X2 = 10, BOMB = 11, EXPLOSION = 12, FLOATING_TEXT = 13 };

// пулинг объектов
public class Pooler : MonoBehaviour {

    // публичные переменные
    public GameObject bullet; // пуля
    public int bulletStartAmount = 10; // количество пуль
    public GameObject feather; // перья
    public int featherStartAmount = 4; // количество перьев
    public GameObject pinkBear, coala, greenGuy; // враги
    public int amountPerEnemy = 2; // для каждого врага
    public GameObject coin, gold, chest, heal, freeze, X2; // бонусы
    public int amountPerBonus = 2; // для каждого бонуса
    public GameObject bomb; // бомба
    public int amountPerBomb = 2; // количество бомб
    public GameObject explosion; // взрыв
    public int amountPerExplostion = 1; // количество взрывов
    public GameObject floatingText; // плавающий текст
    public int amountPerFloatingText = 3; // количество плавающего текста

    // личные переменные
    private List<GameObject> pooledEnumPrefabs; // объекты перечисления
    private List<List<GameObject>> superList; // список списков
    private List<GameObject> bulletList; // список пуль
    private List<GameObject> featherList; // список перьев
    private List<GameObject> pinkBearList; // список розовых медведей
    private List<GameObject> coalaList; // список коал
    private List<GameObject> greenGuyList; // список зелёных чуваков
    private List<GameObject> coinList; // список монет
    private List<GameObject> goldList; // список слитков
    private List<GameObject> chestList; // сисок сундуков
    private List<GameObject> healList; // список лечилок
    private List<GameObject> freezeList; // список замерзаек
    private List<GameObject> X2List; // список двоек
    private List<GameObject> bombList; // список бомб
    private List<GameObject> explosionList; // список взрывов
    private List<GameObject> floatingTextList; // список плавающих текстов
    private static Pooler instance; // ссылка на себя

    // пробуждение
    private void Awake() {

        instance = this;

        // список пуль
        bulletList = new List<GameObject>(bulletStartAmount);
        for (int i = 0; i < bulletStartAmount; i++) {

            GameObject bulletObject = Instantiate(bullet);
            bulletObject.transform.rotation = Quaternion.identity;
            bulletObject.transform.parent = gameObject.transform;
            bulletList.Add(bulletObject);
        }

        // список перьев
        featherList = new List<GameObject>(featherStartAmount);
        for (int i = 0; i < featherStartAmount; i++) {

            GameObject featherObject = Instantiate(feather);
            featherObject.transform.parent = gameObject.transform;
            featherList.Add(featherObject);
        }

        // список врагов
        pinkBearList = new List<GameObject>(amountPerEnemy);
        coalaList = new List<GameObject>(amountPerEnemy);
        greenGuyList = new List<GameObject>(amountPerEnemy);
        for (int i = 0; i < amountPerEnemy; i++) {

            GameObject pinkBearObject = Instantiate(pinkBear);
            GameObject coalaObject = Instantiate(coala);
            GameObject greenGuyObject = Instantiate(greenGuy);
            pinkBearObject.transform.SetParent(gameObject.transform);
            coalaObject.transform.SetParent(gameObject.transform);
            greenGuyObject.transform.SetParent(gameObject.transform);
            pinkBearList.Add(pinkBearObject);
            coalaList.Add(coalaObject);
            greenGuyList.Add(greenGuyObject);
        }

        // список бонусов
        coinList = new List<GameObject>(amountPerBonus);
        goldList = new List<GameObject>(amountPerBonus);
        chestList = new List<GameObject>(amountPerBonus);
        healList = new List<GameObject>(amountPerBonus);
        freezeList = new List<GameObject>(amountPerBonus);
        X2List = new List<GameObject>(amountPerBonus);
        for (int i = 0; i < amountPerBonus; i++) {

            GameObject coinObject = Instantiate(coin);
            GameObject goldObject = Instantiate(gold);
            GameObject chestObject = Instantiate(chest);
            GameObject healObject = Instantiate(heal);
            GameObject freezeObject = Instantiate(freeze);
            GameObject X2Object = Instantiate(X2);
            coinObject.transform.SetParent(gameObject.transform);
            goldObject.transform.SetParent(gameObject.transform);
            chestObject.transform.SetParent(gameObject.transform);
            healObject.transform.SetParent(gameObject.transform);
            freezeObject.transform.SetParent(gameObject.transform);
            X2Object.transform.SetParent(gameObject.transform);
            coinList.Add(coinObject);
            goldList.Add(goldObject);
            chestList.Add(chestObject);
            healList.Add(healObject);
            freezeList.Add(freezeObject);
            X2List.Add(X2Object);
        }

        // список бомб
        bombList = new List<GameObject>(amountPerBomb);
        for (int i = 0; i < amountPerBomb; i++) {

            GameObject bombObject = Instantiate(bomb);
            bombObject.transform.SetParent(gameObject.transform);
            bombList.Add(bombObject);
        }

        // список взрывов
        explosionList = new List<GameObject>(amountPerExplostion);
        for (int i = 0; i < amountPerExplostion; i++) {

            GameObject explosionObject = Instantiate(explosion);
            explosionObject.transform.SetParent(gameObject.transform);
            explosionList.Add(explosionObject);
        }

        // список плавающих текстов
        floatingTextList = new List<GameObject>(amountPerFloatingText);
        for (int i = 0; i < amountPerFloatingText; i++) {

            GameObject floatingTextObject = Instantiate(floatingText);
            floatingTextObject.transform.SetParent(gameObject.transform);
            floatingTextList.Add(floatingTextObject);
        }

        // список списков
        superList = new List<List<GameObject>>();
        superList.Add(bulletList);
        superList.Add(featherList);
        superList.Add(pinkBearList);
        superList.Add(coalaList);
        superList.Add(greenGuyList);
        superList.Add(coinList);
        superList.Add(goldList);
        superList.Add(chestList);
        superList.Add(healList);
        superList.Add(freezeList);
        superList.Add(X2List);
        superList.Add(bombList);
        superList.Add(explosionList);
        superList.Add(floatingTextList);

        // объекты перечисления
        pooledEnumPrefabs = new List<GameObject>();
        pooledEnumPrefabs.Add(bullet);
        pooledEnumPrefabs.Add(feather);
        pooledEnumPrefabs.Add(pinkBear);
        pooledEnumPrefabs.Add(coala);
        pooledEnumPrefabs.Add(greenGuy);
        pooledEnumPrefabs.Add(coin);
        pooledEnumPrefabs.Add(gold);
        pooledEnumPrefabs.Add(chest);
        pooledEnumPrefabs.Add(heal);
        pooledEnumPrefabs.Add(freeze);
        pooledEnumPrefabs.Add(X2);
        pooledEnumPrefabs.Add(bomb);
        pooledEnumPrefabs.Add(explosion);
        pooledEnumPrefabs.Add(floatingText);
    }

    // деструктор
    private void OnDestroy() {

        instance = null;
    }

    // получить объект
    private GameObject GetObject(Pooled type, Vector3 position, Quaternion rotation, bool nativeRotation) {

        foreach (GameObject go in superList[(int)type]) {

            if (!go.activeInHierarchy) {

                go.transform.position = position;
                go.transform.rotation = rotation;
                return go;
            }
        }

        GameObject prefabToInstantiate = pooledEnumPrefabs[(int)type];
        GameObject newInstance = Instantiate(prefabToInstantiate, position, nativeRotation ? prefabToInstantiate.transform.rotation : rotation) as GameObject;
        newInstance.transform.parent = gameObject.transform;
        superList[(int)type].Add(newInstance);
        return newInstance;
    }

    // -------------------------
    // статические копии функций
    // -------------------------

    // получить объект
    public static GameObject GetInstance(Pooled type, Vector3 position, Quaternion rotation) {
        return instance.GetObject(type, position, rotation, false);
    }

    // получить объект
    public static GameObject GetInstanceWithNativeRotation(Pooled type, Vector3 position) {
        return instance.GetObject(type, position, Quaternion.identity, true);
    }

    // сделать потомком
    public static void MakeChild(GameObject child) {

        child.transform.SetParent(instance.transform);
    }
}
