﻿using UnityEngine;
using System.Collections;

// метатель предметов
public class ItemThrower : MonoBehaviour, Damagable {

    // публичные переменные
    public float throwForce = 100.0f; // сила подбрасывания
    public float maxUndestructableTime = 0.5f; // время неубиваемости (чтобы разу не убивалась)
    public int scoreAmount = 50; // количество очков
    public int lifeAmount = 0; // количество жизней
    public bool freeze = false; // замёрзнуть
    public bool X2 = false; // увеличить очки
    public AudioClip pickupSound; // звук подъёма

    // личные переменные
    private Rigidbody rb; // твёрдое тело
    private float landingHeight; // высота приземления
    private bool falling; // флаг падения
    private float curUndestructableTime; // время неубиваемости (чтобы разу не убивалась)

    // пробуждение
    private void Awake() {
        
        rb = gameObject.GetComponent<Rigidbody>();
    }

    // активация
    private void OnEnable() {

        rb.velocity = Vector3.zero;
        curUndestructableTime = maxUndestructableTime;
        falling = true;
        transform.SetParent(SuperSingleton.cam.gameObject.transform);
        rb.AddForce(0.0f, throwForce, 0.0f);
        rb.useGravity = true;
        landingHeight = transform.position.y * 0.5f;
    }

    // фиксированное обновление
    private void FixedUpdate() {

        curUndestructableTime -= Time.fixedDeltaTime;
        if (falling && transform.position.y <= landingHeight) {

            // остановить падение
            falling = false;
            rb.useGravity = false;
            rb.velocity = Vector3.zero;
            transform.parent = null;
        }
    }

    // выстрел в предмет
    void Damagable.OnGotDamage(Collider2D collider, float damage, Vector3 worldPosition, bool explosion) {

        if (curUndestructableTime <= 0.0f) {

            // звук
            if (pickupSound != null) SoundsScript.PlaySound(pickupSound);

            // очки и пр.
            if (scoreAmount > 0) {

                // плавающий текст
                ScoreCounter.AddMoney(scoreAmount);
                GameObject floatingTextObject = Pooler.GetInstance(Pooled.FLOATING_TEXT, transform.position, Quaternion.identity);
                floatingTextObject.SetActive(true);
                floatingTextObject.GetComponent<FloatingText>().SetText(scoreAmount);
            }
            if (lifeAmount > 0) Player.AddLife(lifeAmount);
            Pooler.MakeChild(gameObject);
            gameObject.SetActive(false);

            // замёрзнуть или увеличить очки
            if (freeze) GameController.Freze();
            if (X2) GameController.ScoreBoost();
        }
    }
}
