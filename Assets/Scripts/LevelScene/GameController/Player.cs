﻿using UnityEngine;
using System.Collections;

// делегат гибели
public interface HeroDeadDelegate {

    // герой погиб
    void HeroDead();
}

// параметры игрока
public class Player : MonoBehaviour {

    // публичные переменные
    public Healthbar healthBar; // жизнебар
    public float maxLifeAmount = 100.0f; // максимальное количество жизней
    public GameObject[] cracks; // список трещин
    public HeroDeadDelegate del; // делегат гибели

    // личные переменные
    private static Player instance; // ссылка на себя
    private float curLifeAmount; // текущее количество жизней
    private float crackStep; // количество жизней (относительно) до образования новой трещины
    private int prevCrackAmount; // предыдущее количество трещин

    // пробуждение
    private void Awake() {

        instance = this;
        prevCrackAmount = 0;
        curLifeAmount = Constants.MIN_LIFE_AMOUNT ? 1 : maxLifeAmount;
        crackStep = 1.0f / (cracks.Length + 1);
        SetLifeAmount(curLifeAmount);
    }

    // деструктор
    private void OnDestroy() {

        instance = null;
    }

    // установить количество жизней
    private void SetLifeAmount(float lifeAmount) {

        float ratio = curLifeAmount / maxLifeAmount;
        healthBar.SetRatio(ratio);

        if (crackStep == 0.0f) Debug.LogError("деление на нуль");
        int cracksAmount = Mathf.Min(5, (int)((1.0f - ratio) / crackStep));
        if (cracksAmount > prevCrackAmount) {
            for (int i = prevCrackAmount; i < cracksAmount; i++) {
                cracks[i].SetActive(true);
            }
            SoundsScript.PlaySound(SoundsScript.CRACK);
        }
        if (cracksAmount < prevCrackAmount) {
            for (int i = cracksAmount; i < prevCrackAmount; i++) {
                cracks[i].SetActive(false);
            }
        }
        prevCrackAmount = cracksAmount;

        // игра окончена
        if (lifeAmount <= 0.0f) {

            del.HeroDead();
        }
    }

    // вычесть жизни
    public static void GetDamage(float damage) {

        instance.curLifeAmount -= damage;
        instance.SetLifeAmount(instance.curLifeAmount);
    }

    // добавить жизни
    public static void AddLife(float life) {

        instance.curLifeAmount = Mathf.Min(instance.curLifeAmount + life, instance.maxLifeAmount);
        instance.SetLifeAmount(instance.curLifeAmount);
    }

}
