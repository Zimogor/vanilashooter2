﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

// контроллер управления игрой
public class GameController : MonoBehaviour, WinDelegate, HeroDeadDelegate {

    // публичные переменные
    public FreezePanerlController freezePanel; // панель заморозки
    public static bool paused { get; private set; } // флаг паузы
    public static float ScaledTime; // масштабируемое время
    public static float ScaledTimeSinceLevelStarted; // масштабируемое время
    public WinType winTypeScript; // определение результатов    
    public bool gameOver { private set; get; } // флаг законченности игры
    public bool win { private set; get; } // факт победы
    public Text speedBonusText; // текст бонуса за сложность
    public GameObject X2BonusObject; // иконка бонуса X2
    public float x2TimeMax; // время бонуса x2 (максимальное)
    public List<GameObject> objectToHideAtPause; // список объектов, чтобы их скрыть во время паузы
    public LevelIntro levelIntro; // описание уровня
    public GameObject moneyObject; // отображение заработанных денег

    // статические переменные
    public static float totalMultBonus { private set; get; }
    public const int SURVIVING_RECEDE_LEVELS = 5; // количество уровня отката при поражении

    // личные переменные
    private float speedBonusPeriod = 0.3f; // период роста бонуса за сложность
    private float curSpeedBonusTime; // время роста периода
    private static GameController instance; // ссылка на себя
    private float curTimeScale; // текущий масштаб времени
    private float speedBonus; // бонус за сложность
    private float curX2Time; // время бонуса x2 (текущее)
    private bool x2Flag; // флаг бонуса x2

    // управление корутиной времени
    private float corPeriod = 1.0f; // период обновления корутины
    private float corStep = 0.1f; // шаг изменения масштаба времени за период
    private float startFreezeValue = 0.4f; // начальное значение заморозки

    // пробуждение
    private void Awake() {

        // разное
        totalMultBonus = 0.0f;
        x2Flag = false;
        speedBonus = Utils.GetSpeedBonusForLevel(Loader.levelData.difficulty);
        curSpeedBonusTime = speedBonusPeriod;
        gameOver = false;
        instance = this;
        win = false;
        ScaledTime = 0;
        ScaledTimeSinceLevelStarted = 0;
        curTimeScale = 1.0f;
        levelIntro.SetParams(Loader.levelData.levelWinType, Loader.levelData.difficulty, Loader.levelData.winCondition);

        if (SharedPrefs.isFirstPlayerGame()) {

            // самый первый запуск
#if UNITY_ANDROID            
                    SettingsPanelController.Activate(true);
                    SetPause();
#else
            if (Constants.SETTINGS_PC_ON) {
                SettingsPanelController.Activate(true);
                SetPause();
            }
#endif

            // запуск больше не первый
            SharedPrefs.FirstPlayerGameNoMore();
        }
        else ReleasePause();

        // указатели
        gameObject.GetComponent<Player>().del = this;
    }

    // старт
    private void Start() {

        // тип победы
        winTypeScript.del = this;

        // спрятать деньги не для выживания (учитвается автоскрытие при паузе)
        bool activeMoney = Loader.levelData.levelWinType == LevelWinType.SURVIVING;
        moneyObject.GetComponent<Text>().enabled = activeMoney;
        moneyObject.transform.GetChild(0).gameObject.GetComponent<Image>().enabled = activeMoney;
    }

    // деструктор
    private void OnDestroy() {

        instance = null;
        paused = false;
        Time.timeScale = 1.0f;
    }

    // обновление
    private void Update() {

        // масштабируемое время
        if (paused) {
            ScaledTime = 0;
        }
        else {
            ScaledTime = Time.deltaTime / Time.timeScale;
            ScaledTimeSinceLevelStarted += ScaledTime;

            // бонус за сложность
            curSpeedBonusTime -= Time.deltaTime;
            if (curSpeedBonusTime <= 0.0f) {

                curSpeedBonusTime += speedBonusPeriod;
                speedBonus += 0.01f;
                speedBonusText.text = "X " + speedBonus.ToString("0.00");
            }

            // бонус
            totalMultBonus = speedBonus;
            if (x2Flag) {

                curX2Time -= Time.deltaTime;
                if (curX2Time <= 0.0f) {
                    x2Flag = false;
                    X2BonusObject.SetActive(false);
                }
                else totalMultBonus *= 2.0f;
            }
        }
    }

    // включить паузу
    public void SetPause() {

        // единственный старт паузы
        paused = true;
        UndestructableMusic.SetPause(true);
        Time.timeScale = 0.0f;
        foreach (GameObject go in objectToHideAtPause) {
            go.SetActive(false);
        }
    }

    // выключить паузу в следующем кадре
    private IEnumerator ReleasePauseInNextFrame() {

        yield return 0;
        paused = false;
        UndestructableMusic.SetPause(false);
        Time.timeScale = curTimeScale;
        foreach (GameObject go in objectToHideAtPause) {
            go.SetActive(true);
        }

    }

    // выключить паузу
    public void ReleasePause() {

        StartCoroutine(ReleasePauseInNextFrame());
    }

    // замерзание
    private void FreezePrivate() {

        SoundsScript.PlaySound(SoundsScript.FREEZING);
        float freezeLifeTime = (1.0f - startFreezeValue) * corPeriod / corStep;
        freezePanel.Activate(freezeLifeTime);
        curTimeScale = startFreezeValue;
        StopAllCoroutines();
        StartCoroutine(UpTimeScale());
    }

    // буст очков
    private void ScoreBoosePrivate() {

        X2BonusObject.SetActive(true);
        x2Flag = true;
        curX2Time = x2TimeMax;
    }

    // корутина поднятия масштаба времени
    private IEnumerator UpTimeScale() {

        while (curTimeScale < 1.0f) {

            curTimeScale += corStep;
            curTimeScale = Mathf.Min(curTimeScale, 1.0f);
            Time.timeScale = curTimeScale;
            yield return new WaitForSeconds(corPeriod);
        }
    }

    // делегирование победы
    void WinDelegate.OnWin() {

        // разное
        gameOver = true;
        win = true;
        ScoreCounter.AddMoney(Loader.levelData.winPrize);
        GameOverPanelController.Activate("ПОБЕДА", ScoreCounter.GetScore().ToString(), ScoreCounter.GetMoney().ToString());
        SoundsScript.PlaySound(SoundsScript.GAMEOVERWIN);

        // сохранить данные
        SharedPrefs.AppendScoreAndSave(ScoreCounter.GetScore(), ScoreCounter.GetMoney()); // деньги и очки
        int newDifficulty = SharedPrefs.curProgress.GetDifficultyByWinType(Loader.levelData.levelWinType) + 1;
        SharedPrefs.curProgress.SetDifficultyForWinType(Loader.levelData.levelWinType, newDifficulty); // нарастить сложность
        SharedPrefs.curProgress.MixLevels(Loader.levelData.levelWinType); // перемешать сложность
        SharedPrefs.SaveGameProgress();

        SetPause();
    }

    // делегирование поражения
    void HeroDeadDelegate.HeroDead() {

        // разное
        gameOver = true;
        GameOverPanelController.Activate("ПОРАЖЕНИЕ", ScoreCounter.GetScore().ToString(), ScoreCounter.GetMoney().ToString(), true);
        SoundsScript.PlaySound(SoundsScript.GAMEOVERLOSE);

        // сохранить данные
        if (Loader.levelData.levelWinType == LevelWinType.SURVIVING) {

            // снизить сложность для уровня выживания
            int curLevel = Utils.GetLevelFromCamVelocity(SuperSingleton.cam.GetVelocity());
            int newDifficulty = Mathf.Max(0, curLevel - GameController.SURVIVING_RECEDE_LEVELS);
            SharedPrefs.curProgress.SetDifficultyForWinType(Loader.levelData.levelWinType, newDifficulty);
            SharedPrefs.SaveGameProgress();
        }
        SharedPrefs.AppendScoreAndSave(ScoreCounter.GetScore(), ScoreCounter.GetMoney()); // деньги и очки

        SetPause();
    }

    // -------------------------
    // статические копии функций
    // -------------------------

    // глобальное замерзание
    public static void Freze() {

        instance.FreezePrivate();
    }

    // буст для очков
    public static void ScoreBoost() {        

        instance.ScoreBoosePrivate();
    }
}
