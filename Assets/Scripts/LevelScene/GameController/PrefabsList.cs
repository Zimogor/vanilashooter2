﻿using UnityEngine;
using System.Collections;

// списки префабов
public class PrefabsList : MonoBehaviour {

    // публичные переменные
    public Pooled[] enemies; // список врагов
    public Pooled[] bonuses; // список бонусов

    // личные переменные
    private static PrefabsList instance; // ссылка на себя

    // пробуждение
    private void Awake() {
        instance = this;
    }

    // деструктор
    private void OnDestroy() {
        instance = null;
    }

    // выбрать случайного врага
    private Pooled ChooseEnemyPrivate() {

        int index = Random.Range(0, enemies.Length);
        return enemies[index];
    }

    // выбрать случайный бонус
    private Pooled ChooseItemPrivate() {

        if (Random.value > 0.2f) return Pooled.NONE;
        int index = Random.Range(0, Loader.levelData.levelWinType == LevelWinType.SURVIVING ? 6 : 3);
        return bonuses[index];
    }

    // ----------------------------
    // статические копии функций
    // ----------------------------

    // выбрать случайного врага
    public static Pooled ChooseRandomEnemy() {
        return instance.ChooseEnemyPrivate();
    }

    // выбрать случайный бонус
    public static Pooled ChooseRandomItem() {
        return instance.ChooseItemPrivate();
    }
}
