﻿using UnityEngine;
using System.Collections;

// последовательность скриптов
// 1. статический суперсинглтон всегда первый.
// 2. камера всегда вторая (рассчитываются параметры экрана, к которым много чего привязано)

// сборщик статических указателей
public class SuperSingleton : MonoBehaviour {

    // объекты
    public CameraMover camScriptInput; // камера
    public Camera uiCameraInput; // кривая камера интерфейса
    public Canvas mainCanvasInput; // главный канвас

    // статические указатели
    public static CameraMover cam; // камера
    public static Camera skewdUICamera; // кривая камера интерфейса
    public static Canvas canvas; // главный канвас

    // пробуждение
    private void Awake() {

        cam = camScriptInput;
        skewdUICamera = uiCameraInput;
    }

    // старт
    private void Start() {

        // канвас долго инициализирует свой множитель
        canvas = mainCanvasInput;
    }

    // уничтожение
    private void OnDestroy() {

        cam = null;
        skewdUICamera = null;
        canvas = null;
    }
}
