﻿using UnityEngine;
using System.Collections;

// курсор
public class Cursor : MonoBehaviour {

    // публичные переменные
    public GameObject topRight; // часть курсора
    public GameObject bottomRight; // часть курсора
    public GameObject bottomLeft; // часть курсора
    public GameObject topLeft; // часть курсора    

    // личные переменные
    private CameraMover cam; // ссылка на камеру
    private Vector2 homoScreenPosition; // однородные координаты экрана
    private float scale; // масштаб курсора
    private SpriteRenderer[] sprites = new SpriteRenderer[4]; // спрайты уголков
    private bool redCursor; // флаг красности

    // пробуждение
    private void Awake() {

        // разное
        cam = SuperSingleton.cam;
        redCursor = false;

        // спрайты уголков
        sprites[0] = topRight.GetComponent<SpriteRenderer>();
        sprites[1] = bottomRight.GetComponent<SpriteRenderer>();
        sprites[2] = bottomLeft.GetComponent<SpriteRenderer>();
        sprites[3] = topLeft.GetComponent<SpriteRenderer>();
    }

    // ручное обновление
    public void ParentUpdate() {

        // перекрытие и цвет курсора
        Vector2 v1 = (Vector2)transform.position + new Vector2(scale, scale);
        Vector2 v2 = (Vector2)transform.position + new Vector2(-scale, -scale);
        bool nextRedCursor = Physics2D.OverlapArea(v1, v2) != null;
        if (nextRedCursor != redCursor) {
            redCursor = nextRedCursor;
            foreach (SpriteRenderer sr in sprites) {
                sr.color = redCursor ? Color.red : Color.white;
            }
        }
    }

    // установить координаты
    public void SetPosition(Vector2 position) {

        position.x = Mathf.Clamp(position.x, cam.GetLeftVisibleBorder(), cam.GetRightVisibleBorder());
        position.y = Mathf.Clamp(position.y, cam.GetBottomVisbleBorder(), cam.GetTopVisibleBorder());
        UpdatePosition(position); // только через функцию
    }

    // сдвинуть координаты
    public void MovePosition(Vector2 delta) {

        Vector2 nextPosition = (Vector2)transform.position + delta;
        nextPosition.x = Mathf.Clamp(nextPosition.x, cam.GetLeftVisibleBorder(), cam.GetRightVisibleBorder());
        nextPosition.y = Mathf.Clamp(nextPosition.y, cam.GetBottomVisbleBorder(), cam.GetTopVisibleBorder());
        UpdatePosition(nextPosition); // только через функцию
    }

    // обновить положение на экране и вычислить однородные координаты
    private void UpdatePosition(Vector2 position) {

        Vector3 newPosition = position;
        newPosition.z = -5;
        transform.position = newPosition;
        homoScreenPosition = transform.localPosition;
        homoScreenPosition.x /= cam.GetWidth();
        homoScreenPosition.y /= cam.GetHeight();
    }

    // установить масштаб курсора
    public void SetScale(float scale) {

        this.scale = scale;
        topRight.transform.localPosition = new Vector3(scale, scale, 0.0f);
        bottomRight.transform.localPosition = new Vector3(scale, -scale, 0.0f);
        bottomLeft.transform.localPosition = new Vector3(-scale, -scale, 0.0f);
        topLeft.transform.localPosition = new Vector3(-scale, scale, 0.0f);
    }

    // получить разбросанные координаты
    public Vector2 GetDispersedPos() {

        float xDisp = Random.Range(-scale, scale);
        float yDisp = Random.Range(-scale, scale);
        return transform.position - new Vector3(xDisp, yDisp, 0.0f);
    }

    // получить однородные координаты на экране (от -0.5 до +0.5)
    public Vector2 GetHomoScreenPos() {

        return homoScreenPosition;
    }

}
