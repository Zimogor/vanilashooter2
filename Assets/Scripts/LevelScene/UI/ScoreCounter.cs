﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

// счётчик очков и денег
public class ScoreCounter : MonoBehaviour {

    // публичные переменные
    public Text scoreText; // текст для отображения очков
    public Text moneyText; // текст для отображения денег
    public GameObject alignScoreTarget; // объект для выравнивания денег и очков

    // личные переменные
    private static ScoreCounter instance; // ссылка на себя
    private int score; // количество очков
    private int money; // количество денег
    private int killing; // количество убитых       

    // пробуждение
    private void Awake() {

        instance = this;
        score = 0;
        money = 0;
        killing = 0;
        SetScore(score);
        SetMoney(money);
    }

    // старт
    private void Start() {

        // выровнять объект по кнопке "пауза"
        RectTransform alignRect = alignScoreTarget.transform as RectTransform;
        Vector3 worldPosition = SuperSingleton.skewdUICamera.ScreenToWorldPoint(new Vector3(Screen.width + alignRect.anchoredPosition.x * SuperSingleton.canvas.scaleFactor, Screen.height, transform.localPosition.z));
        Vector3 newPosition = transform.position;
        newPosition.x = worldPosition.x;
        transform.position = newPosition;
    }

    // удаление
    private void OnDestroy() {

        instance = null;
    }

    // отобразить очки
    private void SetScore(int score) {

        scoreText.text = score.ToString();

        // выровнять ширину
    }

    // отобразить денги
    private void SetMoney(int money) {

        moneyText.text = money.ToString();
    }

    // добавить очков
    public static void AddScore(int score) {

        instance.score += score;
        instance.SetScore(instance.score);
    }

    // добавить денег
    public static void AddMoney(int money) {

        instance.money += money;
        instance.SetMoney(instance.money);
    }

    // добавить убитых
    public static void AddKilling(int amount) {

        instance.killing += amount;
    }

    // количество убитых
    public static int GetKilling() {

        return instance.killing;
    }

    // количество очков
    public static int GetScore() {

        return instance.score;
    }

    // количество денег
    public static int GetMoney() {

        return instance.money;
    }
}
