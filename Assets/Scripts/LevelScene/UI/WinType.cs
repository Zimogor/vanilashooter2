﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

// интерфейс победы
public interface WinDelegate {

    // получение повреждений
    void OnWin();
}

// скрипт определения результатов победы / поражения
public class WinType : MonoBehaviour {

    // класс хранения времени
    private class TimeInts {
        public int minutes;
        public int seconds;
        public int dseconds;
        public void MakeEqualTo(TimeInts other) {
            minutes = other.minutes;
            seconds = other.seconds;
            dseconds = other.dseconds;
        }
        public bool IsEqualTo(TimeInts other) {
            return dseconds == other.dseconds && seconds == other.seconds && minutes == other.minutes;
        }
        public string GetTotalString() {
            return string.Format("{0:D2}:{1:D2}:{2:D1}", minutes, seconds, dseconds);
        }
    }

    // публичные переменные
    public WinDelegate del; // делегат победы
    public Sprite[] imagesToShow; // изображения (0 - время, 1 - убей всех)

    // личные переменные
    private Text textToShow; // текст отображения
    private bool delAnswered; // делегату отвечено (чтобы не зациклить)
    private LevelWinType levelWinType; // тип победы

    // для времени
    private float time; // общее время
    private TimeInts curTime, prevTime; // время

    // для убей всех
    private int winKilling, curKilling, prevKilling; // количество жертв

    // пробуждение
    private void Awake() {

        // общее
        delAnswered = false;
        textToShow = transform.GetChild(0).GetComponent<Text>();
        levelWinType = Loader.levelData.levelWinType;

        switch (levelWinType) {
            case LevelWinType.TIMING:

                // для победы временем
                curTime = new TimeInts();
                prevTime = new TimeInts();
                gameObject.GetComponent<Image>().sprite = imagesToShow[0];
                break;

            case LevelWinType.KILLING:

                // для победы жертвами
                gameObject.GetComponent<Image>().sprite = imagesToShow[1];
                break;

            case LevelWinType.SURVIVING:

                // без победы
                gameObject.SetActive(false);
                break;
        }       
    }

    // старт
    private void Start() {

        switch (levelWinType) {
            case LevelWinType.TIMING:

                // для победы временем
                time = Loader.levelData.winCondition;
                TimeToInts(time, curTime);
                prevTime.MakeEqualTo(curTime);
                UpdateTextWithTime(curTime);
                break;

            case LevelWinType.KILLING:

                // для победы жертвами
                winKilling = Loader.levelData.winCondition;
                prevKilling = curKilling = winKilling;
                UpdateTextWithKilling(curKilling);
                break;

            case LevelWinType.SURVIVING:
                break;
        }
    }

    // обновление
    private void Update() {

        switch (levelWinType) {
            case LevelWinType.TIMING:

                // для победы временем
                time -= Time.deltaTime;
                if (time <= 0.0f) {

                    // победа временем
                    time = 0.0f;
                    if (!delAnswered) del.OnWin();
                    delAnswered = true;
                }
                TimeToInts(time, curTime);
                if (!curTime.IsEqualTo(prevTime)) {
                    UpdateTextWithTime(curTime);
                    prevTime.MakeEqualTo(curTime);
                }
                break;

            case LevelWinType.KILLING:

                // для победы жертвами
                curKilling = winKilling - ScoreCounter.GetKilling();
                if (curKilling != prevKilling) {
                    prevKilling = curKilling;
                    if (curKilling <= 0) {

                        // победа жертвами
                        curKilling = 0;
                        if (!delAnswered) del.OnWin();
                        delAnswered = true;
                    }

                    UpdateTextWithKilling(curKilling);
                }
                break;

            case LevelWinType.SURVIVING:
                break;
        }
    }

    // ------------------------------
    // для победы жертвами
    // ------------------------------

    // обновить текст с количеством убитых
    private void UpdateTextWithKilling(int killing) {

        textToShow.text = killing.ToString();
    }

    // ------------------------------
    // для победы временем
    // ------------------------------

    // раскидать время на составляющие
    private void TimeToInts(float time, TimeInts timeInts) {

        timeInts.minutes = (int)(time / 60);
        time %= 60;
        timeInts.seconds = (int)time;
        time -= timeInts.seconds;
        timeInts.dseconds = (int)(time * 10);
    }

    // обновить текст для победы временем
    private void UpdateTextWithTime(TimeInts timeInts) {

        textToShow.text = timeInts.GetTotalString();
    }
}
