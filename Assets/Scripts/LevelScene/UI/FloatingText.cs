﻿using UnityEngine;
using System.Collections;

// плавающий текст
public class FloatingText : MonoBehaviour {

    // публичные переменные
    public TextMesh textMesh;
    public MeshRenderer meshRenderer;
    public float floatVelocity = 1.0f; // скорость парения
    public float timeLife = 3.0f; // время жизни

    // личные переменные
    private float curLife;

    // установить текст
    public void SetText(int score) {

        textMesh.text = "+ " + score.ToString();
    }

    // пробуждение
    private void Awake() {

        meshRenderer.sortingLayerName = "UI";
    }

    // старт
    private void OnEnable() {

        curLife = timeLife;
    }

    // обновление
    private void Update() {

        // время жизни
        curLife -= Time.deltaTime; ;
        if (curLife <= 0.0f) {

            gameObject.SetActive(false);
        }

        // вместить высоту
        transform.Translate(0.0f, floatVelocity * Time.deltaTime, 0.0f);
    }
}
