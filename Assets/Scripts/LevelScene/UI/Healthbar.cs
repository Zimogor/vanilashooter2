﻿using UnityEngine;
using System.Collections;

// жизнебар
public class Healthbar : MonoBehaviour {

    // личные переменные
    private Vector3[] vertsFiller; // буфер вешрин для наполнителя
    private Vector2[] tcoordsFiller; // буфер текстурных координат для наполнителя
    private Mesh meshFiller; // сетка для наполнителя
    private float fillerOriginX, fillerWidth, fillerSkewer, fillerSingleAmount, fillerSkewerTC; // параметры наполнителя

    // пробуждение
    private void Awake() {
        
        float REFERENCE_HEIGHT = 720; // опорная высота

        // размеры заднего фона
        float scaleFactor = SuperSingleton.cam.GetHeight() / REFERENCE_HEIGHT;
        float backgroundHeight = scaleFactor * 50.0f;
        float backgroundWidth = scaleFactor * 500.0f;

        // размеры наполнителя
        fillerOriginX = scaleFactor * 63.0f;
        float fillerOriginY = scaleFactor * 2.0f;
        float fillerEndX = scaleFactor * 493.0f;
        float fillerHeight = scaleFactor * 45.0f;
        float fillerSingleWidth = scaleFactor * 33.0f;
        fillerSkewer = scaleFactor * 23.0f;

        // производные размеры наполнителя
        fillerWidth = fillerEndX - fillerOriginX;
        fillerSingleAmount = fillerWidth / fillerSingleWidth;
        fillerSkewerTC = fillerSkewer / fillerSingleWidth;

        // буфер вершин для заднего фона
        Vector3[] vertsBackground = new Vector3[] {
            new Vector3(0.0f, 0.0f, 0.0f),
            new Vector3(0.0f, backgroundHeight, 0.0f),
            new Vector3(backgroundWidth, backgroundHeight, 0.0f),
            new Vector3(backgroundWidth, 0.0f, 0.0f),
        };

        // буфер вершин для наполнителя
        vertsFiller = new Vector3[] {
            new Vector3(fillerOriginX, fillerOriginY, 0.0f),
            new Vector3(fillerOriginX, fillerOriginY + fillerHeight, 0.0f),
            new Vector3(fillerOriginX + fillerWidth, fillerOriginY + fillerHeight, 0.0f),
            new Vector3(fillerOriginX + fillerWidth - fillerSkewer, fillerOriginY, 0.0f),
        };

        // буфер индексов
        int[] indsBackground = new int[] { 0, 1, 2, 0, 2, 3 };
        int[] indsFiller = new int[] { 0, 1, 2, 0, 2, 3 };

        // текстурные координаты для заднего фона
        Vector2[] tcoordsBackground = new Vector2[] {
            new Vector2(0.0f, 0.0f),
            new Vector2(0.0f, 1.0f),
            new Vector2(1.0f, 1.0f),
            new Vector2(1.0f, 0.0f),
        };

        // текстурные координаты для наполнителя
        tcoordsFiller = new Vector2[] {
            new Vector2(0.0f, 0.0f),
            new Vector2(0.0f, 1.0f),
            new Vector2(fillerSingleAmount, 1.0f),
            new Vector2(fillerSingleAmount - fillerSkewerTC, 0.0f),
        };

        // сетка заднего фона
        GameObject backgroundChild = gameObject.transform.GetChild(0).gameObject;
        Mesh meshBackground = backgroundChild.GetComponent<MeshFilter>().mesh;
        meshBackground.vertices = vertsBackground;
        meshBackground.triangles = indsBackground;
        meshBackground.uv = tcoordsBackground;

        // сетка наполнителя
        GameObject fillerChild = gameObject.transform.GetChild(1).gameObject;
        meshFiller = fillerChild.GetComponent<MeshFilter>().mesh;
        meshFiller.vertices = vertsFiller;
        meshFiller.triangles = indsFiller;
        meshFiller.uv = tcoordsFiller;

        // рендерер сетки заднего фона
        MeshRenderer rb = backgroundChild.GetComponent<MeshRenderer>();
        rb.sortingLayerName = "UI";

        // рендерер сетки наполнителя
        MeshRenderer rf = fillerChild.GetComponent<MeshRenderer>();
        rf.sortingLayerName = "UI";

        // выровнять объект по левому краю
        Vector3 worldPosition = SuperSingleton.skewdUICamera.ScreenToWorldPoint(new Vector3(0.0f, Screen.height, transform.localPosition.z));
        Vector3 newPosition = transform.position;
        newPosition.x = worldPosition.x;
        transform.position = newPosition;
    }

    // установить соотношение жизней
    public void SetRatio(float ratio) {

        // вершины
        vertsFiller[2].x = fillerOriginX + fillerWidth * ratio;
        vertsFiller[3].x = vertsFiller[2].x - fillerSkewer;
        meshFiller.vertices = vertsFiller;

        // текстуры
        tcoordsFiller[2].x = fillerSingleAmount * ratio;
        tcoordsFiller[3].x = fillerSingleAmount * ratio - fillerSkewerTC;
        meshFiller.uv = tcoordsFiller;
    }
}
