﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

// описание уровня при старте
public class LevelIntro : MonoBehaviour {

    // публичные переменные
    public Text text; // текст для отображения

    // личные переменные
    private LevelWinType winType; // тип уровня
    private int levelDifficulty; // сложность уровня
    private int levelAmount; // количество целей (секунды, враги)

    // старт
    private void Start() {

        Destroy(gameObject, 3.0f);

        switch (winType) {
            case LevelWinType.KILLING:
                text.text = Localizator.GetValueForKey("KILL_START0") + levelAmount + Localizator.GetValueForKey("KILL_START1") + levelDifficulty; // TODO
                break;
            case LevelWinType.TIMING:
                text.text = Localizator.GetValueForKey("TIME_START0") + levelAmount + Localizator.GetValueForKey("TIME_START1") + levelDifficulty; // todo
                break;
            case LevelWinType.SURVIVING:
                text.text = Localizator.GetValueForKey("SURVIVE_START");
                break;
        }
    }



    // установить параметры
    public void SetParams(LevelWinType winType, int levelDifficulty, int levelAmount) {

        this.winType = winType;
        this.levelDifficulty = levelDifficulty;
        this.levelAmount = levelAmount;
    }
}
