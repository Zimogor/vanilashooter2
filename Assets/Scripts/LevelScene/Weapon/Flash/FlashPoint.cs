﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

// точка вспышки
public class FlashPoint : MonoBehaviour {

    // публичные переменные
    public GameObject muzzlePrefab; // префаб вспышки    

    // личные переменные
    protected SpriteRenderer sprite; // изображение вспышки
    protected Transform muzzleTransform; // координаты вспышки

    // пробуждение
    private void Awake() {

        GameObject muzzleObject = Instantiate(muzzlePrefab);
        muzzleTransform = muzzleObject.transform;
        sprite = muzzleObject.GetComponent<SpriteRenderer>();
        sprite.enabled = false;
    }

    // старт
    private void Start() {

        gameObject.GetComponent<Image>().enabled = false;
    }

    // обновление
    protected virtual void Update() {

        // позиция
        Vector3 newMuzzePosition = SuperSingleton.cam.ScreenToWorld(transform.position);
        newMuzzePosition.z = 0.0f;
        muzzleTransform.position = newMuzzePosition;
    }

    // -----------------------
    // для переопределения
    // -----------------------

    // выстрел
    public virtual void SingleShot() { }
    // начать вспышку
    public virtual void StartFlash() { }
    // завершить вспышку
    public virtual void StopFlash() { }
}
