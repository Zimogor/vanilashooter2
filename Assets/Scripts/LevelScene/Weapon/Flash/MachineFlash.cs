﻿using UnityEngine;
using System.Collections;

// вспышка для пулемёта
public class MachineFlash : FlashPoint {

    // публичные переменные
    public float rotationVelocity = 180.0f; // скорость вращения
    public float minScale = 0.5f; // минимальный масштаб
    public float maxScale = 0.75f; // максимальный масштаб
    public float scalePeriod = 0.3f; // время мерцания

    // личные переменные
    private float scaleTime; // текущее время мерцания

    // обновление
    protected override void Update() {

        base.Update();
        if (!sprite.enabled) return;

        // вращение
        muzzleTransform.Rotate(0.0f, 0.0f, -rotationVelocity * Time.deltaTime);

        // масштабирование
        scaleTime += Time.deltaTime;
        float scaleFactor = scaleTime % (scalePeriod * 2.0f);
        float scale = scaleFactor <= scalePeriod ? maxScale : minScale;
        muzzleTransform.localScale = new Vector3(scale, scale, scale);
    }

    // старт вспышки
    public override void StartFlash() {

        sprite.enabled = true;
        muzzleTransform.rotation = Quaternion.identity;
        scaleTime = 0.0f;
    }

    // завершить вспышку
    public override void StopFlash() {

        sprite.enabled = false;
    }
}
