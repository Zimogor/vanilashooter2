﻿using UnityEngine;
using System.Collections;

// вспышка тяжёлых оружий
public class HeavyFlash : FlashPoint {

    // публичные переменные
    public float flashTime = 0.5f; // время вспышки
    public float maxScale = 0.7f; // максимальный масштаб

    // личные переменные
    private float shotState; // состояние выстрела (от 0.0f до 1.0f)

    // обновление
    protected override void Update() {

        if (!sprite.enabled) return;
        base.Update();

        // обновить состояние
        shotState += Time.deltaTime / flashTime;

        // вспылка кончилась
        if (shotState > 1.0f) {

            sprite.enabled = false;
            return;
        }

        // масштабирование (квадратное падение)
        float scale = (1.0f - shotState * shotState);
        scale *= maxScale;
        muzzleTransform.localScale = new Vector3(scale, scale, scale);
    }

    // выстрел
    public override void SingleShot() {

        shotState = 0.0f;
        sprite.enabled = true;
    }
}
