﻿using UnityEngine;
using System.Collections;

#pragma warning disable 0414 // variable assigned but not used

// делегат стрельбы
public interface ShootGunDelegate {

    void OnShooting(float damage, bool bombLikeBullets);
}

// оружие
public class Gun : MonoBehaviour {

    // публичные переменные
    public float damagePerBullet = 10.0f; // повреждение от одной пули    
    public float minDispersion = 0.2f; // минимальный разброс
    public float maxDispersion = 0.5f; // максимальный разброс
    public float downDispersion = 1.0f; // спад разброса
    public float shootDelay = 0.1f; // задержка между выстрелами
    public int bulletsPerShot = 1; // пулей на выстрел (1 - не дробовики)
    public float dispersionJump = 0.2f; // скачок дисперсии
    public float recoilAmp = 50.0f; // амплитуда отдачи
    public float recoilTime = 1.0f; // время отдачи
    public float maxVelocity = 10.0f; // максимальная скорость перемещения по экрану
    public bool bombLikeBullets; // пули-бомбы
    public bool cycledSound = false; // зацикленная очередь выстрелов
    public bool overrideScale = false; // перезаписать масштаб

    // статические переменные
    public static float weaponScale; // масштаб оружия

    // личные переменные
    private float weaponMoveFactor = 1.0f; // кусок оружия для движения
    private float xDisp, yDisp; // максимальное смещение оружия
    private float curDispersion; // текущий размер дисперсии
    private float nextShoot; // разрешённое время следующего выстрал
    private bool shootFlag; // удерживание стрельбы
    private float recoil; // состояние отдачи отдачи (от 1.0f-начало до 0.0f-нет отдачи)
    private FlashPoint flashPoint; // точка вспышки
    public ShootGunDelegate shootDelegate; // делегат выстрелов
    private static Gun instance; // ссылка на себя
    private AudioSource shotAudio; // звук выстрелов

    // координаты оружия
    private RectTransform rectTransform; // ссылка на координаты
    private Vector2 basePosition; // базовые координаты
    private Vector2 smoothedPosition; // базовые координаты сглажены

    // пробуждение
    private void Awake() {

        // разное
        instance = this;
        curDispersion = minDispersion;
        rectTransform = gameObject.GetComponent<RectTransform>();
        nextShoot = 0.0f;
        shootFlag = false;
        flashPoint = transform.GetChild(0).gameObject.GetComponent<FlashPoint>();
        shotAudio = gameObject.GetComponent<AudioSource>();
        if (cycledSound) shotAudio.loop = true;
    }

    // старт
    private void Start() {

        // вычисление абсолютного смещения оружия в пикселях
        float scaleFactor = SuperSingleton.canvas.scaleFactor;
        xDisp = rectTransform.rect.width * scaleFactor * weaponMoveFactor * weaponScale;
        yDisp = rectTransform.rect.height * scaleFactor * weaponMoveFactor * weaponScale;

        // отложенный запуск функций
        if (delaySetPositionToStart) SetBasePosition(baseScreenPositionCache, forceImageCache);

        // отдача
        recoil = 0.0f;
        recoilAmp *= scaleFactor;
    }

    // деструктор
    private void OnDestroy() {

        instance = null;
    }

    // фиксированное обновление (функция кадрозависимая)
    private void FixedUpdate() {
        if (GameController.paused) return;

        // сгладить движение
        Vector3 velocity = Vector3.zero;
        smoothedPosition = Vector3.SmoothDamp(smoothedPosition, basePosition, ref velocity, 0.1f);
    }

    // ручное обновление
    public void ParentUpdate() {
        if (GameController.paused) return;

        // создать смещение от отдачи (кубическое)
        Vector2 vRecoilDisp;
        if (recoil > 0.0f) {

            float recoilDisp = Mathf.Pow(recoil, 3) * recoilAmp;
            vRecoilDisp = new Vector2(recoilDisp, recoilDisp);
        }
        else {

            vRecoilDisp = Vector3.zero;
        }

        // изображение
        rectTransform.position = smoothedPosition - vRecoilDisp;   

        // стрельба
        if (shootFlag && GameController.ScaledTimeSinceLevelStarted >= nextShoot) {

            // пучок выстрелов (для пулемёта - 1)
            for (int i = 0; i < bulletsPerShot; i++) {

                shootDelegate.OnShooting(damagePerBullet, bombLikeBullets);
            }

            // скачок дисперсии
            curDispersion += dispersionJump;
            curDispersion = Mathf.Min(curDispersion, maxDispersion);

            // время до следующего выстрела
            nextShoot = GameController.ScaledTimeSinceLevelStarted + shootDelay;

            // разное
            recoil = 1.0f; // отдача
            flashPoint.SingleShot(); // выспышка
            if (!cycledSound) shotAudio.Play(); // звук
        }

        // уменьшение дисперсии
        curDispersion -= GameController.ScaledTime * downDispersion;
        curDispersion = Mathf.Max(curDispersion, minDispersion);

        // отдача
        recoil -= Time.deltaTime / recoilTime;
        recoil = Mathf.Max(recoil, 0.0f);
    }

    // начало стрельбы
    public void StartShooting() {
        if (GameController.paused) return;

        shootFlag = true;
        if (cycledSound) shotAudio.Play();
        flashPoint.StartFlash();
    }

    // конец стрельбы
    public void StopShooting() {

        shootFlag = false;
        if (cycledSound) shotAudio.Stop();
        flashPoint.StopFlash();
    }

    // текущий размер дисперсии
    public float GetDispersion() {

        return curDispersion;
    }

    // установить базовые координаты
    private bool delaySetPositionToStart = false; // отложить установку координат до старта
    private bool forceImageCache; // кэш отложенного параметра
    private Vector3 baseScreenPositionCache; // кэш координат
    public void SetBasePosition(Vector2 baseScreenPosition, bool forceImage) {

        // отложить запуск, если параметры ещё не готовы
        if (xDisp == 0 || yDisp == 0) {

            delaySetPositionToStart = true;
            baseScreenPositionCache = baseScreenPosition;
            forceImageCache = forceImage;
            return;
        }

        basePosition.x = (baseScreenPosition.x - 0.5f) * xDisp;
        basePosition.y = (baseScreenPosition.y - 0.5f) * yDisp;

        // принудить изображение в базовые координаты
        if (forceImage) {

            rectTransform.position = smoothedPosition = basePosition;
        }
    }
}
