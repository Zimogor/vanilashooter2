﻿using UnityEngine;
using System.Collections;

// родить оружие
public class WeaponController : MonoBehaviour, ShootGunDelegate {

    // публичные переменные
    public GameObject weaponParent; // родитель оружия (canvas)
    public GameObject[] weaponList; // список возможных оружий для рождения
    public Cursor cursor; // курсор
    public GameObject alignTarget; // объект для выравнивания курсора при старте    

    // личные переменные
    private Gun gun; // спрайт оружия
    private float weaponScale = 1.1f; // масштаб оружия

    // пробуждение
    private void Awake() {

        // рождение оружия
        int weaponNumber = Loader.levelData.weaponNumber;
        GameObject weaponObject = Instantiate(weaponList[weaponNumber]);
        gun = weaponObject.GetComponent<Gun>();
        float curScale = gun.overrideScale ? weaponObject.transform.localScale.x : weaponScale;
        weaponObject.transform.SetParent(weaponParent.transform);
        weaponObject.transform.SetAsFirstSibling();
        gun.shootDelegate = this;
        weaponObject.transform.localScale = new Vector3(curScale, curScale, curScale);

        Gun.weaponScale = curScale;
    }

    // старт
    private void Start() {       

        // выравнивание курсора
        Vector3 alignPoint = alignTarget.activeInHierarchy ? alignTarget.transform.position : Input.mousePosition;
        cursor.SetPosition(SuperSingleton.cam.ScreenToWorld(alignPoint));
        cursor.SetScale(gun.GetDispersion());

        // синхронизировать оружие с курсором
        gun.SetBasePosition(cursor.GetHomoScreenPos(), true);
    }

    // обновление (собирает все данные о вводе управления)
    private void LateUpdate() {

        // размер курсора в зависимости от дисперсии оружия
        cursor.SetScale(gun.GetDispersion());
        cursor.ParentUpdate();

        // координаты оружия в зависимости от координат курсора
        gun.SetBasePosition(cursor.GetHomoScreenPos(), false);

        gun.ParentUpdate();
    }

    // установить координаты
    public void SetPosition(Vector2 position) {
        if (GameController.paused) return;

        cursor.SetPosition(position);
    }

    // сдвинуть координаты
    public void MovePosition(Vector2 delta) {
        if (GameController.paused) return;

        cursor.MovePosition(delta);
    }

    // выстрел из оружия
    void ShootGunDelegate.OnShooting(float damage, bool bombLikeBullets) {

        if (bombLikeBullets) {

            // стрельнуть бомбой
            Vector3 bombPosition = cursor.GetDispersedPos();
            GameObject explosionObject = Pooler.GetInstance(Pooled.EXPLOSION, bombPosition, Quaternion.identity);
            explosionObject.SetActive(true);
            SuperSingleton.cam.Tremble();
            Collider2D[] colliders = Physics2D.OverlapCircleAll(bombPosition, 4.0f);
            foreach (Collider2D c in colliders) {

                // попадание во что-то
                c.gameObject.transform.parent.GetComponent<Damagable>().OnGotDamage(c, damage, transform.position, true);
            }
        }
        else {

            // стрельнуть пулькой
            GameObject bulletObject = Pooler.GetInstance(Pooled.BULLET, cursor.GetDispersedPos(), Quaternion.identity);
            bulletObject.SetActive(true);
            bulletObject.GetComponent<Bullet>().Init(damage);
        }
    }

    // втопить курок
    public void StartShooting() {

        gun.StartShooting();
    }

    // вытопить курок
    public void StopShooting() {

        gun.StopShooting();
    }
}
