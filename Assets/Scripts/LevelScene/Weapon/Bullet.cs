﻿using UnityEngine;
using System.Collections;

// интерфейс получения повреждения
public interface Damagable {

    // получение повреждений
    void OnGotDamage(Collider2D collider, float damage, Vector3 worldPosition, bool explosion);
}

// самоуничтожиться
public class Bullet : MonoBehaviour {

    // публичные переменные
    public float opaqueTime = 1.0f; // время непрозрачности
    public float fadeTime = 0.3f; // время затухания
    public SpriteRenderer rend; // изображение пули

    // личные переменные
    private float curTime; // текущее время

    // установить параметры инициализации
    public void Init(float damage) {

        // инициализация
        curTime = opaqueTime + fadeTime;
        Invoke("SelfDestroy", opaqueTime + fadeTime);

        // проверка попадания
        Collider2D collider = Physics2D.OverlapPoint(transform.position);
        if (collider != null) {

            // попадание во что-то           
            collider.gameObject.transform.parent.GetComponent<Damagable>().OnGotDamage(collider, damage, transform.position, false);
        }
    }

    // обновление
    private void Update() {

        // вычисление затухания
        curTime -= Time.deltaTime;

        float alpha = 1.0f;
        if (curTime < fadeTime) {
            alpha = curTime / fadeTime;
        }
        rend.color = new Color(1.0f, 1.0f, 1.0f, alpha);
    }

    // самоуничтожение
    private void SelfDestroy() {

        gameObject.SetActive(false);
    }

    // деактивация
    private void OnDisable() {

        CancelInvoke();
    }
}
