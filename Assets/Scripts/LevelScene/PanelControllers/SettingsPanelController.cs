﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

// управление панелью настроек
public class SettingsPanelController : MonoBehaviour {

    // публичные переменные
    public Sprite[] musicOnSprites; // спрайты кнопки настройки музыки (0 - on, 1 - off)
    public Sprite[] soundOnSprites; // спрайты кнопки настройки звуков (0 - on, 1 - off)
    public GameObject settingsPanel; // сама панель
    public Image musicOnImage; // кнопка настройки музыки
    public Image soundOnImage; // кнопка настройки звуков
    public Image rightControlImage; // изображение правого управления
    public Image leftControlImage; // изображение левого управления
    public Image stickControlImage; // изображение управления стиком
    public Image swipeControlImage; // изображение управления свайпом
    public GameObject topResumeButton; // верхняя кнопка старта
    public GameObject bottomResumeButton; // нижняя кнопка старта

    // личные переменне
    private static SettingsPanelController instance; // ссылка на себя    

    // пробуждение
    private void Awake() {

        instance = this;
        if (delayActivation) Activate(firstTimeCache); 

        // начальная подсветка кнопок
        musicOnImage.sprite = musicOnSprites[SharedPrefs.musicOn ? 0 : 1];
        soundOnImage.sprite = soundOnSprites[SharedPrefs.soundOn ? 0 : 1];

        // начальная подсветка кнопок управления оружием
        if (SharedPrefs.moveLeftShootRight) {

            rightControlImage.color = Color.white;
            leftControlImage.color = Color.gray;
        }
        else {

            leftControlImage.color = Color.white;
            rightControlImage.color = Color.gray;
        }

        // начальная подсветка кнопка управлениям стиком
        if (SharedPrefs.stickControltype) {

            stickControlImage.color = Color.white;
            swipeControlImage.color = Color.gray;
            stickControlImage.gameObject.transform.GetChild(0).GetComponent<Text>().color = Color.white;
            swipeControlImage.gameObject.transform.GetChild(0).GetComponent<Text>().color = Color.gray;
        }
        else {

            swipeControlImage.color = Color.white;
            stickControlImage.color = Color.gray;
            swipeControlImage.gameObject.transform.GetChild(0).GetComponent<Text>().color = Color.white;
            stickControlImage.gameObject.transform.GetChild(0).GetComponent<Text>().color = Color.gray;
        }
    }

    // уничтожение
    private void OnDestroy() {

        instance = null;
    }

    // кнопка "soundOn"
    public void OnSoundOnButtonClick() {

        if (soundOnImage.sprite == soundOnSprites[0]) {

            // выключение звуков
            soundOnImage.sprite = soundOnSprites[1];
            AudioListener.volume = 0;
            SharedPrefs.SaveSoundOn(false);
        }
        else {

            // включение звуков
            soundOnImage.sprite = soundOnSprites[0];
            AudioListener.volume = 1;
            SharedPrefs.SaveSoundOn(true);
        }
    }

    // кнопка "musicOn"
    public void OnMusicOnButtonClick() {

        if (musicOnImage.sprite == musicOnSprites[0]) {

            // выключение музыки
            musicOnImage.sprite = musicOnSprites[1];
            SharedPrefs.SaveMusicOn(false);
            UndestructableMusic.MusicOn(false);
        }
        else {

            // включение музыки
            musicOnImage.sprite = musicOnSprites[0];
            SharedPrefs.SaveMusicOn(true);
            UndestructableMusic.MusicOn(true);
        }
    }

    // кнопка "SWIPE"
    public void OnSwipeTouch() {

        if (swipeControlImage.color.r > 0.9f) {

            // повторный тычок (ничего не делать)
            return;
        }

        InputChooser.SetControlType(false);
        SharedPrefs.SaveControlType(false);
        swipeControlImage.color = Color.white;
        stickControlImage.color = Color.gray;
        swipeControlImage.gameObject.transform.GetChild(0).GetComponent<Text>().color = Color.white;
        stickControlImage.gameObject.transform.GetChild(0).GetComponent<Text>().color = Color.gray;
    }

    // кнопка "STICK"
    public void OnStickTouch() {

        if (stickControlImage.color.r > 0.9f) {

            // повторный тычок (ничего не делать)
            return;
        }

        InputChooser.SetControlType(true);
        SharedPrefs.SaveControlType(true);
        stickControlImage.color = Color.white;
        swipeControlImage.color = Color.gray;
        stickControlImage.gameObject.transform.GetChild(0).GetComponent<Text>().color = Color.white;
        swipeControlImage.gameObject.transform.GetChild(0).GetComponent<Text>().color = Color.gray;
    }

    // кнопка "RIGHT"
    public void OnRightTouch() {

        if (rightControlImage.color.r > 0.9f) {

            // повторный тычок (ничего не делать)
            return;
        }

        SharedPrefs.SaveControlSideType(true);
        rightControlImage.color = Color.white;
        leftControlImage.color = Color.gray;
    }

    // кнопка "LEFT"
    public void OnLeftTouch() {

        if (leftControlImage.color.r > 0.9f) {

            // повторный тычок (ничего не делать)
            return;
        }

        SharedPrefs.SaveControlSideType(false);
        leftControlImage.color = Color.white;
        rightControlImage.color = Color.gray;
    }

    // ------------------------------
    // внешнее взаимодействие
    // ------------------------------

    // активировать панель (определяет какую из двух кнопок отображать)
    private static bool delayActivation = false; // отложить активацию до инициализации
    private static bool firstTimeCache; // кэш первого раза (если не удалось активировать)
    public static void Activate(bool firstTime) {

        // отложить запуск при нулевом указателе
        if (instance == null) {

            delayActivation = true;
            firstTimeCache = firstTime;
            return;
        }

        delayActivation = false;
        instance.settingsPanel.SetActive(true);
        instance.topResumeButton.SetActive(!firstTime);
        instance.bottomResumeButton.SetActive(firstTime);
    }

    // деактивировать панель
    public static void Deactivate() {

        instance.settingsPanel.SetActive(false);
    }
}
