﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

// панель окончания игры (или паузы)
public class GameOverPanelController : MonoBehaviour {

    // публичные переменные
    public GameObject panel;
    public Text mainText; // тект в заголовке
    public Text scoreText; // текст очков на панели
    public Text moneyText; // текст денег на панели
    public Image resumeButtonImage; // кнопка "возобновить" на панели
    public Sprite replayButtonSprite; // перегирать вместо играть

    // личные переменные
    private static GameOverPanelController instance; // ссылка на себя

    // пробуждение
    private void Awake() {
        instance = this;
    }

    // деструктор
    private void OnDestroy() {
        instance = null;
    }

    // активация
    private void ActivatePrivate(string mainText, string scoreText, string moneyText, bool replay) {

        panel.SetActive(true);
        this.scoreText.text = scoreText;
        this.moneyText.text = moneyText;
        this.mainText.text = mainText;
        if (replay) resumeButtonImage.sprite = replayButtonSprite;
    }

    // закрыть панель в следующем кадре
    IEnumerator CloseGameOverPanelInNextFrame() {

        yield return 0;
        panel.SetActive(false);
    }

    // -------------------------
    // статические копии функций
    // -------------------------

    // активация
    public static void Activate(string mainText, string scoreText, string moneyText, bool replay = false) {

        instance.ActivatePrivate(mainText, scoreText, moneyText, replay);
    }

    // активация
    public static void Activate() {

        instance.panel.SetActive(true);
    }

    // деактивация
    public static void Deactivate(bool nextFrameOnly) {

        if (nextFrameOnly) instance.StartCoroutine(instance.CloseGameOverPanelInNextFrame());
        else instance.panel.SetActive(false);
    }
}
