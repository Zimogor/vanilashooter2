﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

// панель замерзания
public class FreezePanerlController : MonoBehaviour {

    // публичные переменные
    public Image[] images; // угловые изображения

    // личные переменные
    private float lifeTime; // время до окончания
    private float maxLifeTime; // полное время жизни

    // активировать
    public void Activate(float lifeTime) {

        gameObject.SetActive(true);
        this.lifeTime = maxLifeTime = lifeTime;        
    }

    // обновить
    private void Update() {

        lifeTime -= Time.deltaTime;
        if (lifeTime <= 0) {

            gameObject.SetActive(false);
        }
        else {

            Color newColor = new Color(1.0f, 1.0f, 1.0f, lifeTime / maxLifeTime);
            foreach (Image i in images) {
                i.color = newColor;
            }
        }
    }
}
