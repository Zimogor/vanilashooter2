﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

// ряд для таблицы
public struct TableRow {
    public TableRow(string name, string score) {
        this.name = name;
        this.score = score;
    }
    public string name; // имя
    public string score; // счёт
}

// таблица таблицы рекордов
public class Table : MonoBehaviour {

    // публичные переменные
    public Text[] numberTexts; // место для номера
    public Text[] nameTexts; // место для имён
    public Text[] scoreTexts; // место для очков
    public Image[] winPics; // призовые изображения (0 - золото, 1 - серебро, 2 - бронза)

    // заполнить значениями (index - позиция, с которой нужно есть)
    public void FeedWithNames(TableRow[] rows, int index) {

        // заполнить номерами
        for (int i = 0; i < 10; i++) {
            int number = (i + index + 1);
            numberTexts[i].text = number.ToString() + ".";
        }
        bool firstPage = index == 0;
        foreach (Image im in winPics) {
            im.enabled = firstPage;
        }
        for (int i = 0; i < 3; i++) {
            numberTexts[i].enabled = !firstPage;
        }

        if (rows == null) return;

        // количество полей к заполнению
        int amount = Mathf.Min(rows.Length - index, nameTexts.Length);

        // заполнение полными значениями
        for (int i = 0; i < amount; i++) {
            nameTexts[i].text = rows[i + index].name;
            scoreTexts[i].text = rows[i + index].score;
        }

        // заполнение пустыми значениями
        for (int i = Mathf.Max(0, amount); i < 10; i++) {
            nameTexts[i].text = "";
            scoreTexts[i].text = "";
        }
    }
}
