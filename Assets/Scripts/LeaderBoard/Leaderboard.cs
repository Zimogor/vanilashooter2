﻿using UnityEngine;
using System.Collections;
using System.Xml;
using UnityEngine.UI;

// панель лидеров
public class Leaderboard : MonoBehaviour, RadioPressDelegate, SwipeDelegate {

    // публичные переменные
    public SwipeDetector swipeDetector; // определитель свайпов
    public Table tableNext; // следующая таблица
    public Table tablePrev; // предыдущая таблица
    public RadioButtons radioButtons; // радиокнопки
    public Text leaderName; // имя лидера
    public Text leaderScore; // очки лидера
    public GameObject notificationPanel; // панель уведомлений
    public Text notificationText; // текст уведомления
    public Text sharedName; // имя для "поделиться"
    public GameObject sharePanel; // панель "поделиться"
    public Text scoreText; // количество очков

    // личные переменные
    private Animation tableNextAnimation; // анимация следующей таблицы
    private Animation tablePrevAnimation; // анимация предыдущей таблицы
    private int curNumber; // текущая таблица
    private int prevNumber; // предыдущая таблица
    private TableRow[] rows; // непосредственно данные
    private const int ROWS_PER_TABLE = 10; // строк на таблицу
    private const int MAX_ROWS_TOTAL_AMOUNT = 50; // максимум всех строк
    private HorDirection prevDirection = HorDirection.RIGHT; // направление последней анимации

    // пробуждение
    private void Awake() {

        swipeDetector.del = this;
        curNumber = 0;
        prevNumber = -1;
        tableNextAnimation = tableNext.GetComponent<Animation>();
        tablePrevAnimation = tablePrev.GetComponent<Animation>();
        radioButtons.del = this;
    }

    // активация
    private void OnEnable() {

        // панель уведомлений
        notificationPanel.SetActive(true);
        notificationText.text = Localizator.GetValueForKey("CONNECTION");

        // поделиться результатом
        if (SharedPrefs.lastSavedScore < GlobalScoreCounter.GetScore()) {

            // пора сохранить новые очки
            sharePanel.SetActive(true);
            scoreText.text = GlobalScoreCounter.GetScore().ToString();
        }
        else {

            // новых очков нет, запрос таблицы рекордов
            sharePanel.SetActive(false);
            StartCoroutine(GetLeaderbooardRequest());
        }
    }

    // ответ о запросе таблицы рекордов
    private IEnumerator GetLeaderbooardRequest() {

        string url = "http://sazaha.bget.ru/vanila_leaderboard.php";
        WWW www = new WWW(url);
        yield return www;
        if (www.error != null) {

            // ошибка
            notificationPanel.SetActive(true);
            notificationText.text = Localizator.GetValueForKey("NO_CONNECTION");
        }
        else {

            // скрыть панель
            notificationPanel.SetActive(false);

            // распарсить ответ
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(www.text);
            XmlNodeList nodeList = xmlDoc.SelectNodes("result/row");
            int arraySize = Mathf.Min(nodeList.Count, MAX_ROWS_TOTAL_AMOUNT);
            rows = new TableRow[arraySize];
            for (int i = 0; i < arraySize; i++) {
                string name = nodeList[i].SelectSingleNode("name").InnerText;
                string score = nodeList[i].SelectSingleNode("score").InnerText;
                rows[i] = new TableRow(name, score);
            }

            // лидер
            if (rows.Length > 0) {
                leaderName.text = rows[0].name;
                leaderScore.text = rows[0].score.ToString();
            }

            // накормить таблицы
            if (prevDirection == HorDirection.RIGHT) {
                tableNext.FeedWithNames(rows, curNumber * ROWS_PER_TABLE);
                if (prevNumber != -1) tablePrev.FeedWithNames(rows, prevNumber * ROWS_PER_TABLE);
            }
            if (prevDirection == HorDirection.LEFT) {
                tableNext.FeedWithNames(rows, prevNumber * ROWS_PER_TABLE);
                if (prevNumber != -1) tablePrev.FeedWithNames(rows, curNumber * ROWS_PER_TABLE);
            }
        }
    }

    // ответ о запросе на добавление записи
    private IEnumerator PostAddNewEntry(string id, string name, string score) {

        string url = "http://sazaha.bget.ru/vanila_add_new_entry.php";
        WWWForm wwwForm = new WWWForm();
        wwwForm.AddField("name", name);
        wwwForm.AddField("score", score);
        wwwForm.AddField("id", id);
        WWW www = new WWW(url, wwwForm);
        yield return www;
        if (www.error != null) {

            // ошибка
            notificationPanel.SetActive(true);
            notificationText.text = Localizator.GetValueForKey("NO_CONNECTION");
        }
        else {

            // сохранить сохранённое количество очков
            SharedPrefs.SaveLastSavedScore(GlobalScoreCounter.GetScore());

            // запрос таблицы рекордов
            StartCoroutine(GetLeaderbooardRequest());
        }
    }

    // кнопка "поделиться"
    public void OnShareButtonClick() {

        if (sharedName.text == "") return;

        // загрыть панель и запустить запрос
        sharePanel.SetActive(false);
        string id = SharedPrefs.GetLeaderBoardID();
        string name = sharedName.text;
        string score = GlobalScoreCounter.GetScore().ToString();
        StartCoroutine(PostAddNewEntry(id, name, score));
    }

    // нажатие на радиокнопки
    void RadioPressDelegate.OnPressed(int number, HorDirection direction) {

        // анимация вправо
        prevNumber = curNumber;
        curNumber = number;
        if (direction == HorDirection.RIGHT) {

            tableNextAnimation.Play("TableNextForward");
            tablePrevAnimation.Play("TablePrevForward");

            // накормить таблицы
            tableNext.FeedWithNames(rows, curNumber * ROWS_PER_TABLE);
            tablePrev.FeedWithNames(rows, prevNumber * ROWS_PER_TABLE);
        }

        // анимация влево
        if (direction == HorDirection.LEFT) {

            tableNextAnimation.Play("TableNextBack");
            tablePrevAnimation.Play("TablePrevBack");

            // накормить таблицы
            tableNext.FeedWithNames(rows, prevNumber * ROWS_PER_TABLE);
            tablePrev.FeedWithNames(rows, curNumber * ROWS_PER_TABLE);
        }
        prevDirection = direction;
    }

    // определение свайпов
    void SwipeDelegate.Swipe(HorDirection direction) {

        if (direction == HorDirection.LEFT && curNumber < 4)
            radioButtons.OnButtonClick(curNumber + 1);
        if (direction == HorDirection.RIGHT && curNumber > 0)
            radioButtons.OnButtonClick(curNumber - 1);
    }
}
