﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

// делегат нажатий на кнопки
public interface RadioPressDelegate {

    // нажатие
    void OnPressed(int number, HorDirection direction);
}

// радиокнопки
public class RadioButtons : MonoBehaviour {

    // публичные переменные
    public Image[] buttons; // массив изображений кнопок
    public Sprite[] buttonSprites; // спрайты кнопок (0 - пустой, 1- полный)
    public RadioPressDelegate del; // делегат нажатий

    // личные переменные
    private int curButton; // активная кнопка

    // пробуждение
    private void Awake() {

        curButton = 0;
        buttons[curButton].sprite = buttonSprites[1];
    }

    // нажатие на кнопку
    public void OnButtonClick(int number) {

        // звук
        SoundsScript.PlaySound(SoundsScript.BUTTON_CLICK);

        // внешний вид
        if (number == curButton) return;
        HorDirection direction = number > curButton ? HorDirection.RIGHT : HorDirection.LEFT;
        buttons[curButton].sprite = buttonSprites[0];
        curButton = number;
        buttons[curButton].sprite = buttonSprites[1];

        // делегат
        if (del != null) del.OnPressed(number, direction);
    }
}
