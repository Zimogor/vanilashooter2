﻿using UnityEngine;
using System.Collections;
using System.Text.RegularExpressions;
using UnityEngine.UI;

// смещатель каретки (исправление бага)
public class CaretBugFixer : MonoBehaviour {

    // публичные переменные
    public int heightDisplacement = 14; // величина смещения
    public Canvas canvas; // канвас
    public InputField inputField; // текст ввода

    // пробуждение
    private void Awake() {

        inputField.onValidateInput += ValidateInput;

        // для истории (отключение белой полоски в inputText на андроиде)
        // inputField.shouldHideMobileInput = true;
        // http://forum.unity3d.com/threads/move-ui-above-touchkeyboard-for-mobile.353493/
    }

    // старт
    private void Start() {

        StartCoroutine(MoveCaret());
    }

    // разрешённые символы
    public char ValidateInput(string text, int charIndex, char addedChar) {

        var newChar = Regex.Replace(addedChar.ToString(), @"[^a-zA-Z0-9а-яА-Я ]", "");
        return (newChar == null || newChar.Length == 0 || newChar == "") ? '\0' : newChar[0];
    }

    // сдвинуть каретку
    IEnumerator MoveCaret() {
        yield return 0;

        Transform caret = gameObject.transform.FindChild("InputField Input Caret");
        if (caret == null) yield break;
        RectTransform caretTransform = caret as RectTransform;
        if (caretTransform == null) yield break;
        Vector3 position = caretTransform.anchoredPosition;
        position.y = heightDisplacement * canvas.scaleFactor;
        caretTransform.anchoredPosition = position;
    }
}
