﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

// обработка для кнопки "В МЕНЮ"
public class LevelMenuInput : MonoBehaviour {

    // публичные переменные
    public static LevelMenuInput instance { get; private set; } // ссылка на себя
    public GameController gameController; // игровой контроллер
    public Image settingsButtonClose; // изображение кнопки "закрыть" на панели "settings"
    public RectTransform pauseButtonTransform; // кнопка "пауза"
    public GameObject settingsButton; // кнопка "настройки"

    // личные переменные
    private float pauseWidth, pauseHeight; // размерность кнопки "пауза"

    // пробуждение
    private void Awake() {

        instance = this;

        // отключить настройки на PC
#if !UNITY_ANDROID
        settingsButton.SetActive(Constants.SETTINGS_PC_ON);
#endif
    }

    // старт
    private void Start() {

        // размеры кнопки "пауза"
        float scaleFactor = SuperSingleton.canvas.scaleFactor;
        pauseWidth = scaleFactor * pauseButtonTransform.rect.width;
        pauseHeight = scaleFactor * pauseButtonTransform.rect.height;
    }

    // деструктор
    private void OnDestroy() {

        instance = null;
    }

    // попадание в кнопку паузы
    private bool IsButtonPausePrivate(Vector2 point) {

        return point.x >= pauseButtonTransform.position.x && point.x <= pauseButtonTransform.position.x + pauseWidth && point.y <= pauseButtonTransform.position.y && point.y >= pauseButtonTransform.position.y - pauseHeight;
    }
    public static bool IsButtonPause(Vector2 point) {

        return instance.IsButtonPausePrivate(point);
    }

    // -----------------------------
    // GAME OVER PANEL
    // -----------------------------

    // кнопка "пауза"
    public void OnGameOverClick() {

        string buttonCatption = Localizator.GetValueForKey("PAUSE");
        GameOverPanelController.Activate(buttonCatption, ScoreCounter.GetScore().ToString(), ScoreCounter.GetMoney().ToString());
        gameController.SetPause();
    }

    // кнопка "меню"
    public void OnMenuClick() {

        // сохранить деньги при выходе из паузы
        if (!gameController.gameOver) SharedPrefs.AppendScoreAndSave(ScoreCounter.GetScore(), ScoreCounter.GetMoney()); // деньги и очки

        // снизить сложность для surviving
        if (Loader.levelData.levelWinType == LevelWinType.SURVIVING && !gameController.gameOver) {

            int curLevel = Utils.GetLevelFromCamVelocity(SuperSingleton.cam.GetVelocity());
            int newDifficulty = Mathf.Max(0, curLevel - GameController.SURVIVING_RECEDE_LEVELS);
            SharedPrefs.curProgress.SetDifficultyForWinType(Loader.levelData.levelWinType, newDifficulty);
            SharedPrefs.SaveGameProgress();
        }

        // загрузить меню
        Loader.LoadMenuFromLevel();
        gameController.ReleasePause();
        GameOverPanelController.Deactivate(true);
    }

    // кнопка "настройки"
    public void OnSettingsClick() {

        SettingsPanelController.Activate(false);
        GameOverPanelController.Deactivate(false);
    }

    // кнопка "возобновить"
    public void OnResumeButtonClick() {

        // перезапустить уровень
        if (gameController.gameOver) {

            // следующий уровень
            LevelSurrounding winSurrounding = SharedPrefs.curProgress.GetSurroundingByWinType(Loader.levelData.levelWinType);
            LevelSurrounding loseSurrounding = Loader.levelData.levelSurrounding;
            LevelSurrounding levelSurrounding = gameController.win ? winSurrounding : loseSurrounding;

            // разное
            int difficulty = SharedPrefs.curProgress.GetDifficultyByWinType(Loader.levelData.levelWinType);
            Loader.LevelData.Builder builder = new Loader.LevelData.Builder(levelSurrounding, Loader.levelData.levelWinType);
            builder.SetWinParams(Utils.GetConditionForLevel(difficulty, Loader.levelData.levelWinType), Utils.GetPrizeForLevel(difficulty));
            builder.SetDiffParams(Loader.levelData.weaponNumber, difficulty);
            Loader.LevelData levelData = new Loader.LevelData(builder);
            Loader.LoadLevelFromLevel(levelData);
            return;
        }

        // возобновление после паузы        
        GameOverPanelController.Deactivate(false);
        SettingsPanelController.Deactivate();
        gameController.ReleasePause();
    }

    // -----------------------------
    // SETTINGS PANEL
    // -----------------------------    

    // кнопка "закрыть"
    public void OnCloseSettingsButton() {

        if (gameController.gameOver) {

            // закрыть и вернуть экран окончания игры
            GameOverPanelController.Activate();
            SettingsPanelController.Deactivate();
        }
        else {

            // закрыть и возобновить игру
            OnResumeButtonClick();
        }
    }
}
