﻿using UnityEngine;
using System.Collections;

#pragma warning disable 0414 // variable assigned but not used

// решение о выборе управления
public class InputChooser : MonoBehaviour {

    // личные переменные
    private StickInput stickInput; // скрипт управления стиком
    private TouchInput touchScript; // скрипт управления свайпом
    private MouseInput mouseScript; // скрипт управления мышью
    private static InputChooser instance; // ссылка на себя

    // пробуждение
    private void Awake() {

        instance = this;
        stickInput = gameObject.GetComponent<StickInput>();
        touchScript = gameObject.GetComponent<TouchInput>();
        mouseScript = gameObject.GetComponent<MouseInput>();
    }

    // деструктор
    private void OnDestroy() {

        instance = null;
    }

    // старт
    private void Start() {

#if UNITY_ANDROID

        if (SharedPrefs.stickControltype) {

            // выбрать андроид (стик)
            stickInput.enabled = true;
        }
        else {

            // выбрать андроид (свайпы)
            touchScript.enabled = true;
        }

#else

        // выбрать мышь
        mouseScript.enabled = true;        

#endif

    }

    // сменить тип управления
    public static void SetControlType(bool stickType) {

#if UNITY_ANDROID

        if (stickType) {

            instance.stickInput.enabled = true;
            instance.touchScript.enabled = false;
        }
        else {

            instance.touchScript.enabled = true;
            instance.stickInput.enabled = false;
        }

#endif

    }
}
