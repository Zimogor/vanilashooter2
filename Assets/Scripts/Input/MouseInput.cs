﻿using UnityEngine;
using System.Collections;

// управление мышью
public class MouseInput : MonoBehaviour {

    // публичные переменные
    public WeaponController weaponController; // управление оружием

    // обновление
    private void Update() {

        // движение
        Vector3 worldPoint = SuperSingleton.cam.ScreenToWorld(Input.mousePosition);
        weaponController.SetPosition(worldPoint);

        // стреляние
        if ((Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1)) && !LevelMenuInput.IsButtonPause(Input.mousePosition)) {

            weaponController.StartShooting();
        }
        else if (Input.GetMouseButtonUp(0) || Input.GetMouseButtonUp(1)) weaponController.StopShooting();
    }
}
