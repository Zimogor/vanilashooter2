﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

// обработка событий главного меню
public class MainMenuInput : MonoBehaviour {

    // публичные переменные
    public GameObject weaponScreen; // экран оружия
    public GameObject loadingScreen; // экран загрузки
    public GameObject leaderboardScreen; // экран лидеров
    public Animator summerButtonAnimator; // аниматор уровня "лето"
    public Animator winterButtonAnimator; // аниматор уровня "зима"
    public Animator forestButtonAnimator; // аниматор уровня "лес"
    public LevelDescription descriptionScript; // скрипт описания уровня
    public Image[] winTypeImages; // кнопки-изображения уровней (0 - forest, 1 - summer, 2 - winter)
    public Sprite[] winTypeSprites; // спрайты для кнопок уровней (0 - time, 1 - survival, 2 - killthemall)

    // личные переменные
    private LevelSurrounding selectedLevelSurrounding; // выбранный уровень
    private LevelWinType levelWinType; // выбранный тип победы
    private int winCondition; // условие победы
    private int winPrize; // премия победы
    private int levelDifficulty; // сложность уровня

    // старт
    private void Start() {

        OnLoadSummerClick();

        // распределить изображения
        winTypeImages[0].sprite = GetSpriteForWinType(SharedPrefs.curProgress.pos0);
        winTypeImages[1].sprite = GetSpriteForWinType(SharedPrefs.curProgress.pos1);
        winTypeImages[2].sprite = GetSpriteForWinType(SharedPrefs.curProgress.pos2);
    }

    // загрузить уровень леса
    public void OnLoadForestClick() {

        // анимация
        forestButtonAnimator.SetBool("Jump", true);
        winterButtonAnimator.SetBool("Jump", false);
        summerButtonAnimator.SetBool("Jump", false);

        // параметры уровня
        CalculateLevelParams(LevelSurrounding.FOREST, SharedPrefs.curProgress.pos0);
    }

    // загрузить уровень лета
    public void OnLoadSummerClick() {

        // анимация
        summerButtonAnimator.SetBool("Jump", true);
        forestButtonAnimator.SetBool("Jump", false);
        winterButtonAnimator.SetBool("Jump", false);

        // параметры уровня
        CalculateLevelParams(LevelSurrounding.SUMMER, SharedPrefs.curProgress.pos1);
    }

    // загрузить уровень зимы
    public void OnLoadWinterClick() {

        // анимация
        winterButtonAnimator.SetBool("Jump", true);
        summerButtonAnimator.SetBool("Jump", false);
        forestButtonAnimator.SetBool("Jump", false);

        // параметры уровня
        CalculateLevelParams(LevelSurrounding.WINTER, SharedPrefs.curProgress.pos2);
    }

    // экран выбора оружия
    public void OnChooseWeaponClick() {

        weaponScreen.SetActive(true);
    }

    // экран лидеров
    public void OnLeaderBoardClick() {

        leaderboardScreen.SetActive(true);
    }

    // кнопка "назад"
    public void OnExitClick() {

        Loader.LoadStartFromMenu();
    }

    // загрузить уровень
    public void OnStartButtonClick() {

        UndestructableMusic.StartGameSound();
        loadingScreen.SetActive(true);
        Loader.LevelData.Builder builder = new Loader.LevelData.Builder(selectedLevelSurrounding, levelWinType);
        builder.SetWinParams(winCondition, winPrize);
        builder.SetDiffParams(weaponScreen.GetComponent<WeaponShop>().GetChosenWeaponNumber(), levelDifficulty);
        Loader.LevelData levelData = new Loader.LevelData(builder);
        Loader.LoadLevelFromMenu(levelData);
    }

    // читерские деньги
    public void OnCheatMoneyClick() {

        if (Constants.CHEAT_BUTTONS) {

            GlobalScoreCounter.AddMoneyAndSave(10000);
        }
    }

    // читерские очки
    public void OnCheatScoreClick() {

        if (Constants.CHEAT_BUTTONS) {

            GlobalScoreCounter.AddScoreAndSave(50);
        }
    }

    // определить изображение для типа победы
    private Sprite GetSpriteForWinType(LevelWinType winType) {

        switch (winType) {

            case LevelWinType.KILLING:
                return winTypeSprites[2];
            case LevelWinType.SURVIVING:
                return winTypeSprites[1];
            case LevelWinType.TIMING:
                return winTypeSprites[0];
        }
        return null;
    }

    // вычислить параметры уровня
    private void CalculateLevelParams(LevelSurrounding levelSurrounding, LevelWinType levelWinType) {

        selectedLevelSurrounding = levelSurrounding;
        this.levelWinType = levelWinType;
        levelDifficulty = SharedPrefs.curProgress.GetDifficultyByWinType(levelWinType);
        winCondition = Utils.GetConditionForLevel(levelDifficulty, levelWinType);
        winPrize = Utils.GetPrizeForLevel(levelDifficulty);
        descriptionScript.SetDescription(levelWinType, levelDifficulty, winPrize, winCondition);
    }

    // ----------------------
    // экран магазина
    // ----------------------

    // выход из экрана выбора оружия
    public void OnShopExitClick() {

        weaponScreen.SetActive(false);
    }

    // выход из экрана лидеров
    public void OnLeaderBoardExitClick() {

        leaderboardScreen.SetActive(false);
    }
}
