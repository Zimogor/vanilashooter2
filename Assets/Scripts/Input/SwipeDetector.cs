﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

// делегат свайпов
public interface SwipeDelegate {

    void Swipe(HorDirection direction);
}

// определитель свайпов
public class SwipeDetector : MonoBehaviour {

    // публичные переменные
    public Image[] outImages; // внесвайповые объекты
    public SwipeDelegate del; // делегат свайпов

    // событие перетаскивания (вызывается триггером)
    public void Dragged(BaseEventData data) {

        // данные
        PointerEventData pointerData = data as PointerEventData;
        Vector3 point = pointerData.position;

        // проверка попадание во внесвайповые объекты
        Vector3[] corners = new Vector3[4];
        foreach (Image i in outImages) {            
            i.rectTransform.GetWorldCorners(corners);
            float minX = corners[0].x;
            float maxX = corners[2].x;
            float minY = corners[0].y;
            float maxY = corners[2].y;
            if (point.x > minX && point.x < maxX && point.y > minY && point.y < maxY) return;
        }

        // определение направления смещения
        float x = pointerData.delta.x;
        float y = pointerData.delta.y;
        if (Mathf.Abs(y) >= Mathf.Abs(x)) {
            
            // игнорировать вертикальные движения
            return;
        }

        // ответ делегату
        del.Swipe(x > 0 ? HorDirection.RIGHT : HorDirection.LEFT);
    }
}
