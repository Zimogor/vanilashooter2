﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

// обработка кнопок стартового экрана
public class StartScreenInput : MonoBehaviour {

    // публичные переменные
    public GameObject copyrightPanel; // панель создателей
    public GameObject cheatButton; // читерская кнопка сброса настроек
    public Image soundOnButton; // кнопка включения звуков
    public Image musicOnButton; // кнопка включения музыки
    public Sprite[] soundOnSprites; // спрайты для кнопки звуков (0 - on, 1 - off)
    public Sprite[] musicOnSprites; // спрайты для кнопки музыки (0 - on, 1 - off)

    // пробуждение
    private void Awake() {

#if UNITY_WEBPLAYER

        // sitelock
        if (!Constants.OFF_SITELOCK_FGL) {
            Application.ExternalEval("if(document.location.host != 'www.flashgamelicense.com' && document.location.host != 'flashgamelicense.com' && document.location.host != 'www.fgl.com' && document.location.host != 'fgl.com') { document.location='http://goo.gl/gAaSdu'; }");
        }
#endif

        // разное
        cheatButton.SetActive(Constants.CHEAT_BUTTONS);
        musicOnButton.sprite = musicOnSprites[SharedPrefs.musicOn ? 0 : 1];
        soundOnButton.sprite = soundOnSprites[SharedPrefs.soundOn ? 0 : 1];
    }

    // кнопка "PLAY"
    public void OnPlayButtonClick() {

        Loader.LoadMenuFromStart();
    }

    // кнопка "copyright"
    public void OnCopyrightButtonClick() {

        copyrightPanel.SetActive(true);
    }

    // выход из "copyright"
    public void OnCopyrightExitClick() {

        copyrightPanel.SetActive(false);
    }

    // читерская кнопка сброса настроек
    public void OnCheatButtonClick() {

        SharedPrefs.ClearAll();
    }

    // кнопка "music on"
    public void OnMusicOnClick() {

        if (musicOnButton.sprite == musicOnSprites[0]) {

            // отключить музыку
            musicOnButton.sprite = musicOnSprites[1];
            SharedPrefs.SaveMusicOn(false);
            UndestructableMusic.MusicOn(false);
        }
        else {

            // включить музыку
            musicOnButton.sprite = musicOnSprites[0];
            SharedPrefs.SaveMusicOn(true);
            UndestructableMusic.MusicOn(true);
        }
    }

    // кнопка "sound on"
    public void OnSoundOnClick() {

        if (soundOnButton.sprite == soundOnSprites[0]) {

            // отключить музыку
            soundOnButton.sprite = soundOnSprites[1];
            AudioListener.volume = 0;
            SharedPrefs.SaveSoundOn(false);
        }
        else {

            // включить музыку
            soundOnButton.sprite = soundOnSprites[0];
            AudioListener.volume = 1;
            SharedPrefs.SaveSoundOn(true);
        }
    }
}
