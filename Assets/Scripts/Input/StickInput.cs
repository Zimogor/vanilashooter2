﻿using UnityEngine;
using System.Collections;

// управление стиком
public class StickInput : MonoBehaviour {

    // публичные переменные
    public float moveSensivity; // чувствительность
    public GameObject stickBack; // фон стика
    public GameObject stick; // стик
    public WeaponController weaponController; // ссылка на оружие
    public Canvas canvas; // канвас

    // личные переменные
    private bool movingFlag; // флаг движения
    private float maxDisplacement = 120; // максимальное отделение стика (без учёта масштаба)
    private float maxDisp; // максимальное отделение стика (с учётом масштаба)

    // пробуждение
    private void Awake() {

        movingFlag = false;
        float scaleFactor = canvas.scaleFactor;
        maxDisp = maxDisplacement * scaleFactor;
    }

    // обновление
    private void Update() {
        if (GameController.paused) {

            // отключить всё для паузы
            movingFlag = false;
            stickBack.SetActive(false);
            return;
        }

        // проследить все тапы
        for (int i = 0; i < Input.touchCount; i++) {

            Touch touch = Input.GetTouch(i);

            // инвертировать управление
            bool moveTouch = touch.position.x > Screen.width * 0.5f;
            if (SharedPrefs.moveLeftShootRight) moveTouch = !moveTouch;

            // управление
            if (moveTouch) {

                switch (touch.phase) {

                    case TouchPhase.Began:

                        // отобразить стик
                        if (LevelMenuInput.IsButtonPause(touch.position)) break;
                        movingFlag = true;
                        stickBack.SetActive(true);
                        stickBack.transform.position = touch.position;
                        stick.transform.position = touch.position;
                        break;

                    case TouchPhase.Ended:

                        // спрятать стик
                        movingFlag = false;
                        stickBack.SetActive(false);
                        break;

                    case TouchPhase.Moved:

                        // подвинуть стик
                        stick.transform.position = touch.position;
                        break;
                }

            }
            else {

                // стрельба
                if (touch.phase == TouchPhase.Began && !LevelMenuInput.IsButtonPause(touch.position)) weaponController.StartShooting();
                else if (touch.phase == TouchPhase.Ended) weaponController.StopShooting();
            }
        }

        // движение курсора
        if (movingFlag) {

            Vector2 disp = stick.transform.position - stickBack.transform.position;

            // ограничить расстояние
            if (disp.magnitude > maxDisp) {
                disp = disp.normalized * maxDisp;
                stick.transform.position = (Vector2)stickBack.transform.position + disp;
            }

            weaponController.MovePosition(disp * moveSensivity * Time.deltaTime);
        }
    }
}
