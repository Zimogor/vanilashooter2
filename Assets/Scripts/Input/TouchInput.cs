﻿using UnityEngine;
using System.Collections;

// управление касаниями
public class TouchInput : MonoBehaviour {

    // публичные переменные
    public float moveSensivity; // чувствительность
    public WeaponController weaponController; // ссылка на оружие

    // обновление
    private void Update() {

        // проследить все тапы
        for (int i = 0; i < Input.touchCount; i++) {

            Touch touch = Input.GetTouch(i);

            // инвертировать управление
            bool moveTouch = touch.position.x > Screen.width * 0.5f;
            if (SharedPrefs.moveLeftShootRight) moveTouch = !moveTouch;

            // управление
            if (moveTouch) {

                // движение
                if (touch.phase == TouchPhase.Moved) weaponController.MovePosition(touch.deltaPosition * moveSensivity);
            }
            else {

                // стрельба
                if (touch.phase == TouchPhase.Began && !LevelMenuInput.IsButtonPause(touch.position)) weaponController.StartShooting();
                else if (touch.phase == TouchPhase.Ended) weaponController.StopShooting();
            }
        }
    }
}
